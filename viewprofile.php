<?php //trace($proposal_status);
use OpenTok\MediaMode;
use OpenTok\OpenTok;
use OpenTok\Session;

$apiKey = '46507192';
$apiSecret = '3db2275caf2d615112a6cdac204c22b9fbed2d22';


$getSession = $this->session->userdata;



//VIEW PROFILE PROPOSAL STATUS
$view_id 								= $this->uri->segment(2);
$view_proposal_info 					= $this->User_model->ProposalStatus( $view_id );


/*
Developer name 	: Edwin
Description 	: Condition for the proposal/accept/decline etc
*/

$view_is_connected 						= '';
$request_pending 						= false;
if( !empty( $view_proposal_info[0]->request_status ) ) {
	$view_proposal_status 				= $view_proposal_info[0]->request_status;
	//IF PENDING STATUS
	if( $view_proposal_status == 'pending' ) {
		$request_pending 				= true;
	}
	if( $view_proposal_status == 'pending' || $view_proposal_status == 'accept' ) {
		
		$view_is_connected 				= 'disabled';

		if( 
			( $view_proposal_info[0]->user_id == $this->session->userdata( 'id' ) ) || 
			( $view_proposal_info[0]->respond_id == $this->session->userdata( 'id' ) ) 
		) {
			$view_is_connected 		= '';
		}

	}
}



//LOGGEDIN USER PROPOSAL STATUS
$loggedin_id 						= $this->session->userdata( 'id' );
$loggedin_proposal_info 			= $this->User_model->ProposalStatus( $loggedin_id );

$loggedin_is_connected 				= '';
$loggedin_request_pending 			= false;

if( !empty( $loggedin_proposal_info[0]->request_status ) ) {

	$loggedin_proposal_status 		= $loggedin_proposal_info[0]->request_status;
	//IF PENDING STATUS
	if( $loggedin_proposal_status == 'pending' ) {

		$loggedin_request_pending	= true;

	}
	if( $loggedin_proposal_status == 'pending' || $loggedin_proposal_status == 'accept' ) {

		$loggedin_is_connected 		= 'disabled';

	}

}

//IF BLOCKED
if( !empty($checkBlock) && $checkBlock[0]->block_by == $getSession['id'] ) {
	$view_is_connected 			= 'disabled';
	$loggedin_is_connected 		= 'disabled';
}


/*
Developer name 	: Edwin
Description 	: IF the user is not activated, WINK, PROPOSAL, GIFT Disabled
*/

if( $this->session->userdata( 'active_status' ) == 0 ) {
	
	$view_is_connected 			= 'disabled';
	$loggedin_is_connected 		= 'disabled';

}

if( $isSameGender == 1 ) {
	$view_is_connected 			= 'disabled';
	$loggedin_is_connected 		= 'disabled';
}


if ($_REQUEST['chat'] == 'video' && !isset($_REQUEST['session_id'])) {
    $opentok = new OpenTok($apiKey, $apiSecret);

    $sessionOptions = array(
        'mediaMode' => MediaMode::ROUTED,
    );

    $session = $opentok->createSession($sessionOptions);
    $sessionId = $session->getSessionId();
    $token = $opentok->generateToken($sessionId);

    $chatUrl = current_url() . '?chat=video&session_id=' . $sessionId . '&token=' . $token;

    if (isset($chatUrl)) {
        $getSession = $this->session->userdata;
        //$notification = "<a href='". $chatUrl . "' class='videocalling'>Accept Call</a> Sent you by ".$getSession['username'];
        $this->User_model->setChatNotification($userdata->user_id, $chatUrl, $getSession['id']);
    }

} else {
    $sessionId = $_REQUEST['session_id'];
    $token = $_REQUEST['token'];
    //unset($_COOKIE['videopopup']);
}
// update data when user praposal expire

if (!empty($sentproposal) && $sentproposal[0]->request_status == "pending") {
	$request_pending 	= true;
    $create_date = $sentproposal[0]->request_sending_time == '' ? $myproposal[0]->request_sending_time : $sentproposal[0]->request_sending_time;
    if (time() - strtotime($create_date) > 60 * 60 * 24 && $sentproposal[0]->id != '') {
        ?>
<script>
    $.ajax({
        url: "<?php echo base_url(); ?>/ajax/ajax/auto_decline_sent_proposal",
        data: "proposal_id=" + <?php echo $sentproposal[0]->id; ?>,
        method: 'post',
        success: function(result) {
            if (result == "done") {
                //window.location.reload();
            }
        }
    });

</script>
<?php //print "Older than 24h";
    } else {
        ?>

<?php
//print "Newer than 24h";
    }
//exit;
}
/*if( $request_pending ) {
	echo "pass";
} else {
	echo "fail";
}
echo $request_pending;*/
?>
<style>
    .block_r .accept {
        width: 100%;
    }

    #more {
        display: none;
    }

    #more2 {
        display: none;
    }

    .select2 {
        width: 100% !important;
    }

    .select2.narrow {
        width: 100% !important;
    }

    .wrap.select2-selection--single {
        height: 100%;
    }

    .select2-container .wrap.select2-selection--single .select2-selection__rendered {
        word-wrap: break-word;
        text-overflow: inherit;
        white-space: normal;
    }

    /*
	 | //lea.verou.me/css3patterns
	 | Because white bgs are boring.
	*/
    /* html {
		 background-color: #fff;
		 background-size: 100% 1.2em;
		 background-image:
			 linear-gradient(
			 90deg,
			 transparent 79px,
			 #abced4 79px,
			 #abced4 81px,
			 transparent 81px
			 ),
			 linear-gradient(
			 #eee .1em,
			 transparent .1em
			 );
	 }*/
    #videos {
        position: relative;
        width: 100%;
        height: 100%;
        margin-left: auto;
        margin-right: auto;
        min-height: 500px;
    }

    #subscriber {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 10;
    }

    #publisher {
        position: absolute;
        width: 130px;
        height: 130px;
        bottom: 0px;
        left: 0px;
        z-index: 100;
        border: 3px solid white;
        border-radius: 3px;
    }

    @media only screen and (max-width: 768px) {


        .margin {
            margin-bottom: 0px !important;
            margin-top: 20px !important;
        }

        .relatedimg .mobhead {
            padding: 0px !important;
            padding-left: 15px !important;
            min-height: 50px !important;
        }
    }

    .color-purple {
        color: #6a007e !important;
        font-weight: 600;
    }

    .low-opacity {
        opacity: 0.5 !important;
    }

    .bg-purple {
        background: #6a007e !important;
        border: 1px solid #6a007e !important;
    }

    .online-point {
        position: absolute;
        left: 58px;
        top: 49px;
    }

    .online-point-2 {
        position: absolute;
        left: 68px;
        top: 57px;
    }

    .hideContent {
        overflow: hidden;
        line-height: 1.5em;
        height: 3em
    }

    .showContent {
        line-height: 1.5em;
        height: auto;
    }

    .showContent {
        height: auto;
    }

    h1 {
        font-size: 24px;
    }

    p {
        padding: 10px 0;
    }

    .show-more {
        padding: 10px 0;
        text-align: center;
        float: right;
        position: relative;
        top: -23px;
        background: #fff;
        padding: 0px;
        right: 20px;
    }

    .show-more a {
        text-decoration: none;
    }

    @media only screen and (max-width: 768px) {
        .show-more {
            right: 0px;
        }

        #profile_propose_wink_gift .prop {
            display: flex !important;
            flex-wrap: wrap !important;
            width: 100% !important;
            justify-content: center !important;
            align-items: center !important;
            align-content: space-around !important;
        }

        #profile_propose_wink_gift .prop li {
            flex-grow: 1 !important;
            width: 33% !important;
        }

    }

    .picture_video_count {
        position: absolute;
        top: 10px;
        left: 25px;
        font-weight: bold;
    }

    .picture_video_count i {
        color: #f00;
        font-size: 16px
    }

    .fancybox-toolbar .fancybox-button--zoom,
    .fancybox-toolbar .fancybox-button--play {
        display: none !important;
    }

    .inner_pro_main_pix .prop li a {
        text-align: center !important;
        border: none !important;
    }

    .inner_pro_main_pix .prop li {
        padding-right: 25px !important;
    }

    .who_am_i_time {
        float: right;
        font-size: 10px !important;
        clear: both !important;
    }

    .color-purple {
        font-size: 14px !important;
    }

    .who_am_i ul li {
        font-size: 11px !important;
    }

    @media only screen and (max-width: 600px) {
        .profile-picure-online .online_b {
            right: 35px !important;
            top: 60px !important;
            font-size: 14px !important;
        }

        .picture_video_count {
            left: 15px;
        }

        .picture_video_count i {
            font-size: 15px
        }

        .inner_pro_main_pix .prop li {
            padding-right: 0px !important;
        }

    }

</style>
<link rel="stylesheet" href="<?php echo base_url() ?>application/views/front/pages/newapp/css/viewprofile.css">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous"> -->
<link rel="stylesheet" href="<?php echo base_url() ?>front/gallery/dist/jquery.fancybox.min.css" />

<button type="button" style="display: none;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="demo_btn">
    Audio modal
</button>

<button type="button" style="display: none;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" id="demo_btn1">
    video modal
</button>
<div class="modal" data-backdrop="" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color:#075A50;">
            <div class="modal-body">
                <div class="module mid">
                    <div class="profile_img text-center">
                        <img style="border-radius: 50%" src="<?= $this->session->userdata( 'call_image' ); ?>" class="rounded rounded-circle person-icon" alt="...">
                    </div>
                    <div class="text-center" style="   color: white;">
                        <h2>Incoming Video Call..</h2>
                    </div>
                    <div id="video_play" style="display: none">
                        <audio id="video_play1" controls>
                            <source src="<?php echo base_url() ?>application/views/front/pages/newapp/audio/old_phone_bell.mp3" type="audio/mp3">
                        </audio>
                    </div>
                    <div class="row call_info "></div>
                    <div class="text-center">
                        <button type="button" class="btn btn-lg  rounded-circle mx-3 text-white" data-dismiss="modal" style="background-color: rgb(236, 33, 33); font-size: 21px; padding: 14px; border-radius:50%; width: 61px; " onClick="reject_video()">
                            <i class="fas fa-phone-slash"></i>
                        </button>
                        <button type="button" class="btn btn-lg  rounded-circle  mx-3 text-white " data-dismiss="modal" style="background-color: rgb(7, 141, 25);border-radius:50%;  font-size: 22px;
                   width: 61px;" onClick="accept_video()">
                            <i class="fa fa-phone"></i>
                        </button>
                        <button type="button" data-dismiss="modal"  id="no_ans_vid" style="display:none"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color:#075A50;">

            <div class="modal-body">
                <div id="audio_play2" style="display: none">
                    <audio id="audio_play22" controls>
                        <source src="<?php echo base_url() ?>application/views/front/pages/newapp/audio/old_phone_bell.mp3" type="audio/mp3">
                    </audio>
                </div>

                <div class="module mid">

                    <div class="profile_img text-center">
                        <img style="border-radius: 50%" src="<?= $this->session->userdata( 'call_image' ); ?>" class="rounded rounded-circle person-icon" alt="...">
                    </div>
                    <div class="text-center" style="   color: white;">
                        <h2>Incoming Audio Call..</h2>
                    </div>
                    <div class="row call_info "></div>
                    <div class="text-center">
                        <button type="button" class="btn btn-lg  rounded-circle mx-3 text-white" data-dismiss="modal" style="background-color: rgb(236, 33, 33); border-radius:50%;     font-size: 21px;
                 padding: 14px;
                 width: 61px;" onClick="reject_audio()">
                            <i class="fas fa-phone-slash"></i>
                        </button>
                        <button type="button" class="btn btn-lg  rounded-circle  mx-3 text-white " data-dismiss="modal" style="background-color: rgb(7, 141, 25);font-size: 22px; border-radius:50%; 
                   width: 61px;" onClick="accept_audio()">
                            <i class="fa fa-phone"></i>
                        </button>
                        <button type="button" data-dismiss="modal"  id="no_ans_aud" style="display:none"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="videos_parent">
    <div id="videos">
        <div id="subscriber"></div>
        <div id="publisher"></div>
    </div>
    <div class="d-flex  shruthi justify-content-center">
        <i class=" fas fa-video  rounded-circle text-black" style="border-radius: 50%" id="vid-btn" title="Video" data-toggle="tooltip" data-placement="top"></i>
        <i class="fas fa-phone-slash rounded-circle   text-white " style="border-radius: 50%" id="cut_calling" onClick="session_destroy()" title="Disconnect" data-toggle="tooltip" data-placement="top"></i>
        <i class="fas fa-microphone rounded-circle  pr-3 text-black" style="border-radius: 50%" id="vol-btn" title="Mute" data-toggle="tooltip" data-placement="top"></i>
    </div>
</div>

<?php

function getage($dob)
{
    $dateOfBirth = $dob;
    $today = date("Y-m-d");
    $diff = date_diff(date_create($dateOfBirth), date_create($today));
    return $diff->format('%y');
}

$status = userstatus($this->uri->segment(2));
$getSession = $this->session->userdata;

//$relation2 = userRelation($getSession['id']);
$accept = false;
//var_dump($relation);exit;
$canSentProposal = true;
if ($this->uri->segment(2) == $getSession['id']) {
    $relation = getuserRelation($getSession['id']);
    if ($relation[0]->status == "commited") {
        $accept = true;
    }
} else {
    $relation = userRelation($this->uri->segment(2));
    if (!empty($relation) && $getSession['id'] > 0) {
        $accept = true;
    }
}
//echo '<pre>';print_r($relation);exit;
$deactive = false;
//echo '<pre>';print_r($userdata);exit;
if ($userdata->active_status == 0 && $userdata->userId > 0) {
    $deactive = true;
}
//echo $deactive;exit;
//echo !$canview .'&&'. $isBlock;exit;
//echo '<pre>';print_r($userdata);exit;
if ((!$canview && $isBlock) || $userdata->status == 3) {?>
<section class="details_page">
    <div class="container">
        <div class="row text-center">
            <!-- This Profile is no longer available for You. . . -->
            <img style="margin: 0px auto;" class="img-responsive" src="<?php echo base_url() ?>front/img/user_not_availbale.png" />
        </div>
    </div>
</section>
<?php } /*elseif ($deactive && $this->uri->segment(2) == $userinfo->userId) {
?>
<section class="details_page">
    <div class="container">
        <div class="row">Your Account has been Deactivated.please activate your account first.<a href="<?php echo base_url(); ?>myprofile">click here</a></div>
    </div>
</section>
<?php
}*/else {
    if ($profileid != $this->uri->segment(2)) {

//        echo '<pre>';print_r($chatUser);exit;
        ?>
<div class="row chat-window col-xs-5 col-md-3 userchat" id="chat_window_1" style="margin-left:10px;display:none;">
    <div class="col-xs-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <div class="col-md-8 col-xs-8">
                    <h3 class="panel-title"><span class="fa fa-comment"></span> <?= $this->lang->line('chat'); ?> - <?php
$chatUser = userinfo($this->uri->segment(2));
        echo ucwords($chatUser[0]->firstname);
        ?> </h3>
                </div>
                <div class="col-md-4 col-xs-4 icon-r" style="text-align: right;">
                    <a href="#"><span id="minim_chat_window" class="fa fa-minus icon_minim"></span></a>
                    <a href="#"><span class="fa fa-times icon_close" data-id="chat_window_1"></span></a>
                </div>
            </div>
            <div class="panel-body msg_container_base" id="all-chat">
                <?php
for ($x = 0; $x < sizeof($broadcast_chat); $x++) {
            if ($broadcast_chat[$x]->user_id == $profileid) {
                // sender self
                ?>
                <div class="row msg_container base_sent" id="self">
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_sent">
                            <p><?php echo $broadcast_chat[$x]->message; ?></p>
                            <time datetime="2009-11-13T20:00"><?php echo lastupdatetime($broadcast_chat[$x]->start_time) ?></time>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-2 avatar">
                        <img src="<?php echo base_url() . userinfo($broadcast_chat[$x]->user_id)[0]->image ?>" class=" img-responsive ">
                    </div>
                </div>

                <?php
} elseif ($broadcast_chat[$x]->user_id == $this->uri->segment(2)) {
                // recver box
                ?>
                <div class="row msg_container base_receive" id="other">
                    <div class="col-md-2 col-xs-2 avatar">
                        <img src="<?php echo base_url() . userinfo($broadcast_chat[$x]->user_id)[0]->image ?>" class=" img-responsive ">
                    </div>
                    <div class="col-md-10 col-xs-10">
                        <div class="messages msg_receive">
                            <p><?php echo $broadcast_chat[$x]->message; ?></p>
                            <time datetime="2009-11-13T20:00"><?php echo lastupdatetime($broadcast_chat[$x]->start_time) ?></time>
                        </div>
                    </div>
                </div>
                <?php
}
        }
        ?>
            </div>
            <div class="panel-footer">
                <div class="input-group">
                    <!--input id="btn-input" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..." /-->

                    <select id="btn-input" type="text" class="form-control select2 narrow wrap broadcast-msg chat_input">

                        <?php
if (!empty($user_broadcast)) {
            for ($m = 0; $m < sizeof($user_broadcast); $m++) {
                echo "<option value=" . $user_broadcast[$m]->id . ">" . $user_broadcast[$m]->email_body . "</option>";
            }
        }
        ?>

                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" id="btn-chat" style="margin-left: 3px;"><?= $this->lang->line('send'); ?></button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<?php }?>
<section class="details_page">
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <div class="inner_pro_top">
                    <!--img src="<?php echo base_url() ?>/front/img/dital_c.png"-->
                    <?php if ($userdata->cover_image != "") {?>
                    <img src="<?php echo base_url() . $userdata->cover_image; ?>">
                    <?php } else {
        ?>
                    <img src="<?php echo base_url() ?>front/img/default_banner.png">
                    <?php }
    ?>

                    <div id="propose_accept_icon">
                        <?php
if (!empty($myproposal) || !empty($sentproposal)) {
        ?>
                        <!--input type="hidden" id="oldtime" value="<?php echo $myproposal[0]->request_sending_time ?>"-->
                        <div class="status"><img src="<?php echo base_url() ?>front/img/broke_heart.png"></div>
                        <?php } elseif (!empty($accepted)) {
        ?>
                        <!--input type="hidden" id="oldtime" value="<?php echo $myproposal[0]->request_sending_time ?>"-->
                        <div class="status"><img src="<?php echo base_url() ?>front/img/joint_heart.png"></div>
                        <?php
}
    ?>
                    </div>


                    <div class="clearfix"></div>
                </div>
                <div class="inner_pro_main_pix">
                    <div class="col-md-3 profile-picure-online">


                        <span <?php if ($userdata->login_status != '1') { echo "style=display:none;"; }?> class="online_b online-userr" onclick="return userOnline()"><i class="fa fa-circle green"></i></span><!-- online -->


                        <span <?php if ($userdata->login_status == '1') { echo "style=display:none;"; }?> class="online_b offline-userr"><i class="fa fa-circle red"></i></span><!-- Offline -->


                        <?php if (isset($gallery_images_public[0]) && !empty($gallery_images_public[0])) {?>
                        <a data-fancybox="gallery" href="<?php echo userImage($userdata->image, $userdata->gender); ?>">
                            <img src="<?php echo userImage($userdata->image, $userdata->gender); ?>" class="condidate_pix">
                        </a>
                        <?php } else {?>
                        <a data-fancybox="gallery" href="<?php echo userImage($userdata->image, $userdata->gender); ?>">
                            <img src="<?php echo userImage($userdata->image, $userdata->gender); ?>" class="condidate_pix">
                        </a>
                        <?php }?>
                    </div>
                    <div class="col-md-9">
                        <div class="condidate_name"><span class="user_name"><?php echo ucwords($userdata->firstname); ?></span>,
                            <span class="user_age"><?php echo calculateAge($userdata->dob); ?></span>
                            <!--<span <?php if ($userdata->login_status != '1') { echo "style=display:none;"; }?> class="online_b online-userr" onclick="return userOnline()"><i class="fa fa-circle green"></i></span>online -->

                            <!--<span <?php if ($userdata->login_status == '1') { echo "style=display:none;"; }?> class="online_b offline-userr"><i class="fa fa-circle red"></i></span> Offline -->

                            <?php 
									$create_date = date('M d,Y H:i:s');
								    if (empty($myproposal) && empty($sentproposal)) {
								        $display = "display:none;";
								    } else {
								        $create_date = $sentproposal[0]->request_sending_time == '' ? $myproposal[0]->request_sending_time : $sentproposal[0]->request_sending_time;
								        //echo $userdata->country_id.' country Name '.$userdata->country_name;
								        $regionSymbol = get_country_name($userdata->country_id)[0]->symbol;
								        $regionHour = get_country_name($userdata->country_id)[0]->diff_hour;
								        $regionMinute = get_country_name($userdata->country_id)[0]->diff_minute;
								        $time_dif = $regionSymbol . $regionHour . ' hour' . $regionSymbol . $regionMinute . ' minutes';

								        //echo '<input type="hidden" id="oldtime" value="' . date('M d,Y H:i:s', strtotime($create_date)) . '">';
								    }
								    echo '<input type="hidden" id="oldtime" value="' . date('M d,Y H:i:s', strtotime($create_date)) . '">';
								    ?>
                            <br>
                            <span id="tr" style="<?php echo $display; ?>"><i class="fa fa-clock-o"></i> <span class="red proposeDiv" id="proposeDiv"></span></span>


                        </div>
                        <?php if ($userdata->writeup == "") {
        ?>
                        <?php echo $userdata->writeup; ?><a href="">Write Something about your self</a>
                        <!--button type="button" id="writeup_section" class="btn btn_edit-new edit_section"><i class="fa fa-edit"></i></button-->
                        <?php
} else {

        //echo $newstr = substr_replace($userdata->aboutuserwriteup, '<span id="dots2">...</span><span id="more2">', 100, 0) . "</span>";

        echo '<div class="content hideContent">' . $newstr = $userdata->aboutuserwriteup . '</div><div class="show-more"><a href="javascript:;">... read more</a></div>';

        ?>

                        <!-- <a href="javascript:void(0)" id="myBtn2"><?= $this->lang->line('read_more'); ?></a> -->

                        <?php }?>
                        <?php if ($deactive && $this->uri->segment(2) == $userinfo->userId) {
        ?>
                        <ul class="prop3">
                            <li><button id="activateBtn" onclick="activateUserPopup()" class="btn btn-info" onmouseover="tooltip.pop(this, '#activate', {offsetY: -20, smartPosition: false})"><span class="propose-nav"><?= $this->lang->line('activate'); ?></span></button></li>

                        </ul>

                        <?php
}?>

                        <div id="profile_propose_wink_gift">
                            <?php //echo "<pre>";print_r($sentproposal[0]->);
    //RESPOND ID NOT EMPTY - PROPOSE ACTIVE                        
    if ($this->uri->segment(2) != $getSession['id'] && !empty($sentproposal[0]->respond_id)) {
        ?>
                            <?php /*?><ul class="prop">

                                <li><a href="#" id="gift_section" class="edit_section" onmouseover="tooltip.pop(this, '#gift', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/gift2.png"> <span class="propose-nav">Send
                                            me gift</span></a></li>
                                <li><a href="#" id="wink_section" class="edit_section" onmouseover="tooltip.pop(this, '#wink', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/wink.png"> <span class="propose-nav">Wink
                                            me</span></a></li>

                            </ul><?php */?>
                            <?php }
	//PROPOSAL ACCEPT						
    if ($accept && $this->uri->segment(2) != $getSession['id']) {
        //echo '<pre>';print_r($relation);exit;
        // echo $getSession['id'] + "is selected";
        ?>
                            <ul class="prop1">
                                <!--<li class="broadcastmsg"><a href="#"><img
												src="<?php echo base_url() ?>/front/img/chat_icon.png"><span
												class="propose-nav"><br> Broadcast</span></a></li>
									<li><a href="#"
										   onmouseover="tooltip.pop(this, '#email', {offsetY: -20, smartPosition: false})"><img
												src="<?php echo base_url() ?>/front/img/email_icon.png"> <span
												class="propose-nav"><br>Email</span></a></li>
									 <li><a href="#"  onclick="video_call('<?php echo $relation[0]->user_id; ?>', '<?php echo $relation[0]->respond_id; ?>')" onmouseover="tooltip.pop(this, '#video', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/video_icon.png"> Video Calling</a></li> -->
                                <!-- <li>
										<a href="<?php echo base_url() ?>application/views/front/pages/newapp/">
										<img src="<?php echo base_url() ?>front/img/video_icon.png">
										<span class="propose-nav">Video Calling</span>
									</a> -->
                                <!-- chat_profile -->
                                <?php $getSession = $this->session->userdata;
        $to_chat_id = $getSession['id'];
        $from_chat_id = $accepted[0]->user_id == $to_chat_id ? $accepted[0]->respond_id : $accepted[0]->user_id;?>
                                <?php
									//if ($userdata->login_status) {
								?>
                                <li>
                                    <a href="<?php echo base_url() ?>front/Frontend/chat_profile/<?php echo $to_chat_id; ?>/<?php echo $from_chat_id; ?>" target="_blank">
                                        <img src="<?php echo base_url() ?>front/img/video_icon.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('call_and_chat'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php 
                                     //}
                                     ?>
                                <li>
                                    <a href="#" id="gift_section" class="edit_section" onmouseover="tooltip.pop(this, '#gift', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/gift2.png">
                                        <div class="propose-nav"><?= $this->lang->line('send_me_gift'); ?></div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" id="wink_section" class="edit_section" onmouseover="tooltip.pop(this, '#wink', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/wink.png">
                                        <div class="propose-nav"><?= $this->lang->line('wink_me'); ?></div>
                                    </a>
                                </li>
                                <?php //}
        /*else{ ?>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>front/Frontend/chat_profile/<?php echo $accepted[0]->user_id; ?>">
                                        <img src="<?php echo base_url() ?>front/img/video_icon.png">
                                        <span class="propose-nav"><br>Video Calling</span>
                                    </a>
                                </li>
                                <?php }*/?>
                                <?php
// if(isset($chatUrl)) {
        // echo '<a href="#" title="' . $chatUrl .'">Chat Url</a>';
        // }

        ?>

                            </ul>
                            <?php
} elseif ($isloggedin && $showbuttons && !$deactive) {
        //var_dump($myproposal);
        ?>

                            <ul class="prop">
                                <?php /*if (($sentproposal[0]->user_id == $this->uri->segment(2) || $myproposal[0]->respond_id == $this->uri->segment(2)) && ($sentproposal[0]->request_status != "pending" || $sentproposal[0]->request_status != "accept" || $myproposal[0]->request_status != "pending" || $myproposal[0]->request_status != "accept")) { ?>
                                <li class="broadcastmsg"><a href="#" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img src="https://projects-iglaps.com/dating-site/front/img/broadcast.png"><span class="propose-nav">Broadcast</span></a></li>
                                <?php } elseif (empty($sentproposal) || empty($myproposal)) {
        ?>
                                <li class="broadcastmsg"><a href="#" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img src="https://projects-iglaps.com/dating-site/front/img/broadcast.png"><span class="propose-nav">Broadcast</span></a></li>
                                <?php }*/
        ?>

                                <?php //print_r($userinfo);
        //print_r($idealmatchcondition);
        $step_jump = $this->Common_Model->step_verify_user_id($getSession["id"]);
		?>

                                <?php /* trace($proposal_status_login);*/
		// echo $proposal_status_login[0]->request_status;
		// echo $face_verified->face_verified;
		if ($face_verified->face_verified == 0) {?>
                                <li class="proposeDiv2">
                                    <a href="#" data-toggle="modal" onclick="showMessage()" data-target="#idealMatchModel" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></div>
                                    </a>
                                </li>
                                <?PHP } else if($proposal_status_login[0]->request_status=='block') {

			if(($proposal_status_login[0]->user_id ==$this->uri->segment(2) || $proposal_status_login[0]->respond_id ==$this->uri->segment(2))){
				//echo $proposal_status_login[0]->request_status;
			}
			else
			{	
				if ($idealmatchcondition && $isSameGender != 1 ) {?>

                                <?php if ($userdata->country_id > 0 && $userinfo->country_id > 0 && !empty($cardInfo) && !empty($usercardInfo)) { 
					if( empty($checkBlock) && $checkBlock[0]->block_by != $getSession['id'] ) {
					?>
                                <li class="proposeDiv2">
                                    <a href="#" id="propose_section" class="edit_section2" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></div>
                                    </a></li>
                                <?php }
					 } else {?>
                                <!--<a href="#" data-toggle="modal" onclick="showMessage()"
						data-target="#idealMatchModel"
						onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img
							src="<?php echo base_url() ?>/front/img/propose.png"> <span
							class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></span></a>-->
                                <li class="proposeDiv2">
                                    <a href="#" id="propose_section" class="edit_section2" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></div>
                                    </a></li>

                                <?php } } else {?>
                                <li class="proposeDiv2">
                                    <a href="#" data-toggle="modal" onclick="addHtml()" data-target="#idealMatchModel" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></div>
                                    </a></li>
                                <?php } 
											   
			}
		} else if ($cansend && !$deactive) { 
			if (!empty($accepted_login_user) || !empty($sentproposal_login_user) || !empty($myproposal_login_user)) {
            // echo date('M d,Y H:i:s',strtotime($myproposal[0]->request_sending_time));
			//echo 'cent '.$sentproposal_login_user[0]->request_status;
			//echo 'my '.$myproposal_login_user[0]->request_status;
            if (!empty($sentproposal_login_user) && $sentproposal_login_user[0]->request_status == "pending") {
               
            } elseif (!empty($myproposal_login_user) && $myproposal_login_user[0]->request_status == "pending") {
               
            }
			elseif (!empty($accepted_login_user) && $accepted_login_user[0]->request_status == "accept") {
               
            }
			}
         else{ ?>
                                <?php if ($idealmatchcondition && $isSameGender != 1) { ?>

                                <?php if ($userdata->country_id > 0 && $userinfo->country_id > 0 && !empty($cardInfo) && !empty($usercardInfo)) { 
					//CHECK USER ALREADY CONNECTED
					if( empty( $view_is_connected ) ) {
				?>
                                <li class="proposeDiv2">
                                    <a href="#" id="propose_section" class="edit_section2" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></div>
                                    </a></li>
                                <?php } ?>
                                <?php } else { ?>
                                <!--<a href="#" data-toggle="modal" onclick="showMessage()"
						data-target="#idealMatchModel"
						onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img
							src="<?php echo base_url() ?>/front/img/propose.png"> <span
							class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></span></a>-->
                                <li class="proposeDiv2">
                                    <a href="#" id="propose_section" class="edit_section2" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></div>
                                    </a></li>

                                <?php }} else { 
					if( empty( $view_is_connected ) ) {
				?>
                                <li class="proposeDiv2">
                                    <a href="#" data-toggle="modal" onclick="addHtml()" data-target="#idealMatchModel" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav"><?= $this->lang->line('propose_to_me'); ?></div>
                                    </a>
                                </li>
                                <?php } ?>
                                <?php }?>


                                <?php }
		} elseif (!empty($sentproposal) || !empty($myproposal)) {
            //echo date('M d,Y H:i:s',strtotime($myproposal[0]->request_sending_time));
            if (!empty($sentproposal) && $sentproposal[0]->request_status == "pending") {
                echo '<input type="hidden" id="oldtime" value="' . date('M d,Y H:i:s', strtotime($sentproposal[0]->request_sending_time)) . '">';
            } elseif (!empty($myproposal) && $myproposal[0]->request_status == "pending") {
                echo '<input type="hidden" id="oldtime" value="' . date('M d,Y H:i:s', strtotime($myproposal[0]->request_sending_time)) . '">';
            }
        } else {
        	if ( $sentproposal[0]->request_status != "pending" && $sentproposal[0]->request_status != "accept" && $sentproposal[0]->request_status != "block" ) {
        		//CHECK USER ALREADY CONNECTED
				if( $loggedin_is_connected != 'disabled' ) {
            	?>
                                <!-- <input type="hidden" id="oldtime" value="2019-02-26 12:15:00"> -->
                                <li class="proposeDiv2">
                                    <a href="#" id="propose_section" class="edit_section2" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})"><img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav"> <?= $this->lang->line('propose_to_me'); ?></div>
                                    </a>
                                </li>

                                <?php
				}
			}
		}
        ?>

                                <?php 

							/*
								Developer name 	: Edwin
								Description 	: Pending/Accept for "GIFT" and "WINK" 
							*/

							if ( 
								$sentproposal[0]->request_status == "pending" 
								|| $sentproposal[0]->request_status == "accept" 
								|| $myproposal[0]->request_status == "pending" 
								|| $myproposal[0]->request_status == "accept" 
							) { ?>

                                <?php
										if( !empty( $view_is_connected ) ) { ?>
                                <li>
                                    <a href="#" style="opacity:0.3">
                                        <img src="<?php echo base_url() ?>/front/img/gift2.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('send_me_gift'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" style="opacity:0.3">
                                        <img src="<?php echo base_url() ?>/front/img/wink.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('wink_me'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php } else { ?>
                                <li>
                                    <a href="#" id="gift_section" class="edit_section" onmouseover="tooltip.pop(this, '#gift', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/gift2.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('send_me_gift'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" id="wink_section" class="edit_section" onmouseover="tooltip.pop(this, '#wink', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/wink.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('wink_me'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>

                                <?php } elseif (
	                                        		( empty( $committedProposal ) ) 
	                                        		&& ( $sentproposal[0]->user_id == $this->uri->segment( 2 ) ) 
	                                        		&& ( $sentproposal[0]->request_status != "pending" 
	                                        				|| $sentproposal[0]->request_status != "accept" 
	                                        				|| $myproposal[0]->request_status != "pending" 
	                                        				|| $myproposal[0]->request_status != "accept" 
	                                        			) 
	                                        		&& ( !empty( $view_is_connected ) ) 
                                        		) { 
                                        ?>

                                <li>
                                    <a href="#" id="gift_section" class="edit_section" onmouseover="tooltip.pop(this, '#gift', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/gift2.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('send_me_gift'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" id="wink_section" class="edit_section" onmouseover="tooltip.pop(this, '#wink', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/wink.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('wink_me'); ?>
                                        </div>
                                    </a>
                                </li>

                                <?php 
										} elseif ( 
													( empty( $sentproposal ) || empty( $myproposal ) )
												) {


            							if ( $request_status || $response_status || $face_verified->face_verified == 0 || !empty( $view_is_connected ) ) {
                						?>
                                <li>
                                    <a href="#" style="opacity:0.3">
                                        <img src="<?php echo base_url() ?>/front/img/gift2.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('send_me_gift'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" style="opacity:0.3">
                                        <img src="<?php echo base_url() ?>/front/img/wink.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('wink_me'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php } else {

											?>
                                <li>
                                    <a href="#" id="gift_section" class="edit_section" onmouseover="tooltip.pop(this, '#gift', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/gift2.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('send_me_gift'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" id="wink_section" class="edit_section" onmouseover="tooltip.pop(this, '#wink', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/wink.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('wink_me'); ?>
                                        </div>
                                    </a>
                                </li>

                                <?php }?>
                                <?php } ?>

                                <?php if (!empty($checkBlock) && $checkBlock[0]->block_by == $getSession['id']) {?>
                                <li>
                                    <!-- <button id="unblock_section"
													onclick="unblockUser('<?php //echo $checkBlock[0]->id ?>', '<?php //echo $checkBlock[0]->user_id ?>', '<?php //echo $checkBlock[0]->respond_id ?>')"
													class="">Unblock
											</button> -->

                                    <a role="button" id="unblock_section" onclick="unblockUser('<?php echo $checkBlock[0]->id ?>', '<?php echo $checkBlock[0]->user_id ?>', '<?php echo $checkBlock[0]->respond_id ?>')" class="">
                                        <img src="<?php echo base_url() ?>/front/img/unblock.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('unblock'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php }?>
                                <br>
                                <div>
                                    <!-- <b>Last Updated:</b> -->
                                    <?php
$updateArray = array($userdata->basic_update, $userdata->match_update, $userdata->other_update, $userdata->user_update, $userdata->user_create, $user_interest->updated_on);
        $max = max(array_map('strtotime', $updateArray));

        $date1 = strtotime(date("Y-m-d H:i:s", $max));
        $date2 = strtotime(date("Y-m-d H:i:s"));
        $diff = abs($date2 - $date1);
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 -
            $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
        //                            printf("%d years, %d months, %d days, %d hours, ". "%d minutes, %d seconds", $years, $months, $days, $hours, $minutes, $seconds);
        if ($years > 0) {
            $str = $years == 1 ? $years . ' year ' : $years . ' years ';
        } elseif ($months > 0) {
            $str = $months == 1 ? $months . ' month ' : $months . ' months ';
        } elseif ($days > 0) {
            $str = $days == 1 ? $days . ' day ' : $days . ' days ';
        } elseif ($hours > 0) {
            $str = $hours == 1 ? $hours . ' hour ' : $hours . ' hours ';
        } elseif ($minutes > 0) {
            $str = $minutes == 1 ? $minutes . ' minute ' : $minutes . ' minutes ';
        } elseif ($seconds > 0) {
            $str = $seconds == 1 ? $seconds . ' second ' : $seconds . ' seconds ';
        } else {
            $str = '1 second';
        }
        //echo $str . ' ago update';
        ?>
                                    <?php
if ($userdata->update_log != '') {
            //echo $userdata->update_log;
        }
        ?>
                                </div>
                            </ul>
                            <?php
} elseif (!$isloggedin) {
        ?>
                            <ul class="prop">
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#loginModel" onmouseover="tooltip.pop(this, '#propose', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/propose.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('propose_to_me'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#loginModel" onmouseover="tooltip.pop(this, '#gift', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/gift2.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('send_me_gift'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#loginModel" onmouseover="tooltip.pop(this, '#wink', {offsetY: -20, smartPosition: false})">
                                        <img src="<?php echo base_url() ?>/front/img/wink.png">
                                        <div class="propose-nav">
                                            <?= $this->lang->line('wink_me'); ?>
                                        </div>
                                    </a>
                                </li>
                                <br />
                                <div>
                                    <!--                                        <b>Last Updated:</b>-->
                                    <?php
$updateArray = array($userdata->basic_update, $userdata->match_update, $userdata->other_update, $userdata->user_update, $userdata->user_create, $user_interest->updated_on);
        $max = max(array_map('strtotime', $updateArray));

        $date1 = strtotime(date("Y-m-d H:i:s", $max));
        $date2 = strtotime(date("Y-m-d H:i:s"));
        $diff = abs($date2 - $date1);
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 -
            $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
        //                            printf("%d years, %d months, %d days, %d hours, ". "%d minutes, %d seconds", $years, $months, $days, $hours, $minutes, $seconds);
        if ($years > 0) {
            $str = $years == 1 ? $years . ' year ' : $years . ' years ';
        } elseif ($months > 0) {
            $str = $months == 1 ? $months . ' month ' : $months . ' months ';
        } elseif ($days > 0) {
            $str = $days == 1 ? $days . ' day ' : $days . ' days ';
        } elseif ($hours > 0) {
            $str = $hours == 1 ? $hours . ' hour ' : $hours . ' hours ';
        } elseif ($minutes > 0) {
            $str = $minutes == 1 ? $minutes . ' minute ' : $minutes . ' minutes ';
        } elseif ($seconds > 0) {
            $str = $seconds == 1 ? $seconds . ' second ' : $seconds . ' seconds ';
        } else {
            $str = '1 second';
        }
        //echo $str . ' ago update';
        ?>
                                    <?php
if ($userdata->update_log != '') {
            //echo $userdata->update_log;
        }
        ?>
                                </div>
                            </ul>


                            <?php
} elseif (!$showbuttons) {
        ?>
                            <ul class="prop">

                            </ul>


                            <?php
}
    ?>
                            <div style="display:none; margin-top:50px">

                                <div id="propose">
                                    <?= $this->lang->line('profile_disabled_24hrs'); ?>
                                    <?= $this->lang->line('cannot_send_receive'); ?>
                                </div>

                            </div>

                            <div style="display:none; margin-top:50px">

                                <div id="gift">
                                    <?= $this->lang->line('profile_disabled_24hrs'); ?>
                                    <?= $this->lang->line('cannot_send_receive'); ?>
                                </div>

                            </div>

                            <div style="display:none; margin-top:50px">

                                <div id="wink">
                                    <?= $this->lang->line('profile_disabled_24hrs'); ?>
                                    <?= $this->lang->line('cannot_send_receive'); ?>
                                </div>

                            </div>

                        </div><!-- profile_proposal_wink_gift -->

                    </div>

                    <div class="clearfix"></div>
                </div>
                <?php
	if ($devicestatus == 1) {
       	if (!empty($accepted) || !empty($myproposal) || !empty($sentproposal)) {
    ?>
                <div class="row hidden-md hidden-lg">
                    <div class="col-md-12">
                        <div class="proposal-details mobile-propose-details">
                            <?php

            	$data_mobile_details[ 'accepted' ] 		= $accepted;
            	$data_mobile_details[ 'myproposal' ] 	= $myproposal;
            	$data_mobile_details[ 'sentproposal' ] 	= $sentproposal;
            	$data_mobile_details[ 'sessionUserId' ]	= $sessionUserId; 
            	

            	echo $this->load->view( 'front/pages/mobile_propose_details', $data_mobile_details, true );
            	?>
                        </div>
                        <!--proposal end-->
                    </div>
                </div>

                <?php }}?>

                <script src="<?= base_url(); ?>front/carousel/carouselengine/amazingcarousel.js"></script>
                <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>front/carousel/carouselengine/initcarousel-0.css">
                <script src="<?= base_url(); ?>front/carousel/carouselengine/initcarousel-0.js"></script>

                
                <div class="row1 photo-thumb">

                    <?php
                        $all_images = array();

                        $all_images[] = array( "link" => userImage($userdata->image, $userdata->gender), "img" => userImage($userdata->image, $userdata->gender), "class" => "" );

                        if ( empty($gallery_videos_public) && empty($gallery_videos_private) ) {
                            $all_images[] = array( "link" => "javascript:void(0);", "class" => "", "img" => base_url() . "front/img/video.png"  );
                        }

                    //if ( !empty($gallery_images_public) || !empty($gallery_images_private) || !empty($gallery_videos_public) || !empty($gallery_videos_private) ) {

                        
                        if( !empty( $gallery_images_public ) ) {
                            for ($i = 0; $i < sizeof($gallery_images_public); $i++) {
                                $all_images[] = array( "link" => base_url() . 'front/imagegallery/' . $userdata->userId . '/public_images/' . $gallery_images_public[$i], "img" => base_url() . 'front/imagegallery/' . $userdata->userId . '/public_images/' . $gallery_images_public[$i], "class" => "" );
                            }
                        }

                        $accept_class = 'blur_image';
                        if( $accept ) {
                            $accept_class = '';
                        }
                        if( !empty( $gallery_images_private ) ) {
                            for ($i = 0; $i < sizeof($gallery_images_private); $i++) {
                                $all_images[] = array( "link" => base_url() . 'front/imagegallery/' . $userdata->userId . '/private_images/' . $gallery_images_private[$i], "img" => base_url() . 'front/imagegallery/' . $userdata->userId . '/private_images/' . $gallery_images_private[$i], "class" => $accept_class );
                            }
                        }
                       
                        //$all_videos = array();
                        if( !empty( $gallery_videos_public ) ) {
                            for ($i = 0; $i < sizeof($gallery_videos_public); $i++) {
                                $all_images[] = array( "link" => base_url() . 'front/videogallery/' . $userdata->userId . '/public_videos/' . $gallery_videos_public[$i], "class" => "", "img" => base_url() . "front/img/video.png"  );
                            }
                        }
                        if( !empty( $gallery_videos_private ) ) {
                            for ($i = 0; $i < sizeof($gallery_videos_private); $i++) {
                                $all_images[] = array( "link" => base_url() . 'front/videogallery/' . $userdata->userId . '/private_videos/' . $gallery_videos_private[$i], "class" => $accept_class, "img" => base_url() . "front/img/video.png" );
                            }
                        }
                        

                        //echo '<pre>'; print_r($all_images);exit;
                        ?>

                        <div class="relatedimg">
                            <h2>Gallery</h2>
                        </div>

                        <div id="amazingcarousel-container-0">
                            <div id="amazingcarousel-0" style="display:none;position:relative;width:100%;max-width:900px;margin:0px auto 0px;">
                                <div class="amazingcarousel-list-container">
                                    <ul class="amazingcarousel-list">
                                        <?php
                                        if( !empty( $all_images ) ) {
                                            foreach( $all_images as $key => $image ) { 
                                                ?>
                                                <li class="amazingcarousel-item">
                                                    <div class="amazingcarousel-item-container">
                                                        <div class="amazingcarousel-image"><a href="<?= ( ( $image[ 'class' ] ) ? 'javascript:void(0);' : $image[ 'link' ] ); ?>" title="1"  class="html5lightbox" data-group="amazingcarousel-0"><img class="<?= ( ( $image[ 'class' ] ) ? $image[ 'class' ] : '' ); ?>" src="<?= $image[ 'img' ]; ?>"  alt="1" style="height: 200px; width: 360px;" /></a></div>                    
                                                    </div>
                                                </li>
                                            <?php
                                            } 
                                        } ?>
                                    </ul>
                                    <div class="amazingcarousel-prev"></div>
                                    <div class="amazingcarousel-next"></div>
                                </div>
                                <div class="amazingcarousel-nav"></div>
                            </div>
                        </div>
                        <?php
                    //}

                    ?>         

                    
                </div>

                
                <?php
$intrests = json_decode($user_interest->interests);
    $user_languages = json_decode($user_interest->languages);

    if ($devicestatus == 0) {
        ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="intrests_icon intrest_icon">
                            <!--                            <h3><?= $this->lang->line('what_i_like'); ?> …</h3>-->
                            <h3>What do I like?</h3>
                            <ul class="blog">
                                <?php foreach ($hobbies_data as $key => $value) {
            ?>
                                <li class="<?php
if (!userIntrest($value->hobbies_name, $intrests)[3]) {
                echo "hide";
            }
            ?>">
                                    <div class="box"><img src="<?php echo $value->image; ?>">
                                        <span><?php echo $value->hobbies_name; ?></span></div>
                                </li>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php }?>

                <!--end-->

                <!--i am looking for-->
                <div class="row">
                    <div class="col-md-12 text-center pix_app"><img src="<?php echo base_url() ?>/front/img/male.png" class="pix_m"></div>

                    <div class="col-md-6">
                        <div class="who_is_my">
                            <h2><?= $this->lang->line('who_is_my_ideal_match'); ?> ? </h2>

                            <ul class="ul_b">
                                <?php
if (empty($idealmatch)) {
        echo '<li>' . $this->lang->line('open') . '</li>';
    } else {
        foreach ($idealmatch as $key => $matchdata) {
            ?>


                                <?php if (!empty($matchdata->kids_name)) {
                ?>
                                <li><?php echo $matchdata->kids_name; ?></li>
                                <?php }
            ?>

                                <?php if (!empty($matchdata->relation_name)) {
                ?>
                                <li><?php echo $matchdata->relation_name; ?></li>
                                <?php }
            ?>
                                <?php if (!empty($matchdata->ageslab_name)) {
                ?>
                                <li><?php echo $matchdata->ageslab_name; ?></li>
                                <?php }
            ?>
                                <?php if (!empty($matchdata->religion_name)) {
                ?>
                                <li><?php echo $matchdata->religion_name; ?></li>
                                <?php }
            ?>
                                <?php if (!empty($matchdata->profession_name)) {
                ?>
                                <li><?php echo $matchdata->profession_name; ?></li>
                                <?php }
            ?>
                                <?php if (!empty($matchdata->education_name)) {
                ?>
                                <li><?php echo $matchdata->education_name; ?></li>
                                <?php }
            ?>
                                <?php if (!empty($matchdata->race_name)) {
                ?>
                                <li><?php echo $matchdata->race_name; ?></li>
                                <?php }
            ?>
                                <?php }}?>
                            </ul>

                            <p style="color: #FF5349;" class="m_padd">
                                <img src="<?php echo base_url() ?>/front/img/icon.png">
                                <?= $this->lang->line('we_only_send_proposal'); ?>
                            </p>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="what_iam">
                            <h2><?= $this->lang->line('what_i_am_looking_for'); ?> ?</h2>
                            <p>
                                <?php //profile.phpecho "<pre>";print_r($userdata);
    if (!empty($userdata->match_writeup)) {
        echo $newstr = substr_replace($this->User_model->getMatchwriteup($userdata->match_writeup), '<span id="dots">...</span><span id="more">', 250, 0) . "</span>";
    }?>
                            </p>
                            <p class="text-right"><a href="javascript:void(0)" id="myBtn"><?= $this->lang->line('read_more'); ?></a></p>
                        </div>
                    </div>

                </div>
                <!--end-->

                <?php
if ($devicestatus == 1) {
        ?>
                <div class="row">
                    <div class="col-md-12">
                        <!--Who am I ?-->

                        <div class="who_am_i">

                            <h2><?= $this->lang->line('who_am_i'); ?> ? <?php //echo $userdata->created_days ?></h2>
                            <?php $user_languages = json_decode($user_interest->languages);?>
                            <ul>
                                <?php if (!empty($userdata->city_name) || !empty($userdata->state_name)) {
            ?>

                                <li>
                                    <span class="color-purple"><?= $this->lang->line('lives_in'); ?>:</span> <?php echo $userdata->city_name; ?>
                                    , <?php echo $userdata->state_name; ?>
                                    <?php
            if (isset($userlogs['Basic info'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userlogs['Basic info']['updated_on']) . '</span>';
                }
            }
            ?></li><?php }?>


                                <!-- 2 PROFESSION -->

                                <?php if (!empty($userdata->user_profession)) {
            ?><li><span class="color-purple"><?= $this->lang->line('profession'); ?>:</span> <?php echo $userdata->user_profession; ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_profession_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>


                                <!-- 3 BODY TYPE -->

                                <?php if (!empty($userdata->body_shape)) {
            ?><li><span class="color-purple"><?= $this->lang->line('body_type'); ?>:</span> <?php echo $userdata->body_shape ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil"></i>&nbsp;' . lastupdatetime($userdata->body_shape_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>



                                <!-- 4 BODY TYPE -->

                                <?php if (!empty($userdata->height)) {
            ?><li><span class="color-purple"><?= $this->lang->line('height'); ?>:</span> <?php echo $userdata->height; ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->height_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>


                                <!-- 5 RELATIONSHIPS -->

                                <?php if (!empty($userdata->user_relationship)) {
            ?><li><span class="color-purple"><?= $this->lang->line('relationship'); ?>:</span> <?php echo $userdata->user_relationship; ?>
                                    <?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_relationship_updated_on) . '</span>';
                }
            }
            ?>
                                </li><?php }?>


                                <!-- 6 RELIGION -->

                                <?php if (!empty($userdata->user_religion)) {
            ?><li><span class="color-purple"><?= $this->lang->line('religion'); ?>:</span> <?php echo $userdata->user_religion ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_religion_updated_on) . '</span>';
                }
            }
            ?></li>
                                <?php } ?>


                                <!-- 7 RACE -->

                                <?php if (!empty($userdata->user_race)) {
            ?><li><span class="color-purple"><?= $this->lang->line('race'); ?>:</span> <?php echo $userdata->user_race; ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_race_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>


                                <!-- 8 KIDs -->

                                <?php if (!empty($userdata->user_kids)) {
            ?><li><span class="color-purple"><?= $this->lang->line('have_kids'); ?>:</span> <?php echo $userdata->user_kids; ?>
                                    <?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_kids_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>


                                <!-- 9 EDUCATION -->

                                <?php if (!empty($userdata->user_education)) {
            ?><li><span class="color-purple"><?= $this->lang->line('education'); ?>:</span> <?php echo $userdata->user_education; ?>
                                    <?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_education_updated_on) . '</span>';
                }
            }
            ?>
                                </li><?php }?>


                                <!-- 10 LANGUAGE -->

                                <?php if (!empty($user_languages)) {
            ?><li><span class="color-purple"><?= $this->lang->line('know'); ?>:</span> <?php echo implode(", ", $user_languages); ?>
                                    <?php
if (isset($userlogs['Social Habits'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($user_interest->languages_updated_on) . '</span>';
                }
            }
            ?>
                                </li><?php }?>


                                <!-- 11 HAIR -->

                                <?php if (!empty($userdata->hair)) {
            ?><li><span class="color-purple"><?= $this->lang->line('hair'); ?>:</span> <?php echo $userdata->hair ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->hair_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>


                                <!-- 12 EYES -->

                                <?php if (!empty($userdata->eye)) {
            ?><li><span class="color-purple"><?= $this->lang->line('eyes'); ?>:</span> <?php echo $userdata->eye ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->eye_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>


                                <!-- 13 DRINK -->

                                <?php if (!empty($user_interest->drinking)) {
            ?><li><span class="color-purple"><?= $this->lang->line('drink'); ?>:</span> <?php echo $user_interest->drinking; ?><?php
if (isset($userlogs['Social Habits'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($user_interest->drinking_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>


                                <!-- 14 SMOKING -->


                                <?php if (!empty($user_interest->smoking)) {
            ?><li><span class="color-purple"><?= $this->lang->line('smoke'); ?>:</span> <?php echo $user_interest->smoking; ?><?php
if (isset($userlogs['Social Habits'])) {
                echo '<span class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($user_interest->smoking_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>



                            </ul>

                        </div>

                        <!--end-->
                    </div>
                </div>
                <?php }?>

                <?php
if ($devicestatus == 1) {
        ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="intrests_icon intrest_icon">
                            <!--                            <h3><?= $this->lang->line('what_i_like'); ?> …</h3>-->
                            <h3>What do I like?</h3>
                            <ul class="blog">
                                <?php foreach ($hobbies_data as $key => $value) {
            ?>
                                <li class="<?php
if (!userIntrest($value->hobbies_name, $intrests)[3]) {
                echo "hide";
            }
            ?>">
                                    <div class="box"><img src="<?php echo $value->image; ?>">
                                        <span><?php echo $value->hobbies_name; ?></span></div>
                                </li>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>

            <div class="col-md-4">
            <?php

    if ($isMyProfile == 1) {
        ?>
            
                <?php 

		if ( $devicestatus == 0 ) {

		?>
                <div id="web-proposal-accepted">
                    <?php
	            if ( !empty( $accepted ) ) {
	                	
	            	$data_int_accepted[ 'accepted' ] 		= $accepted;
	            	$data_int_accepted[ 'sessionUserId' ] 	= $sessionUserId;
	            	$data_int_accepted[ 'disabled' ] 		= $disabled;
	            	echo $this->load->view( 'front/pages/proposal_accepted', $data_int_accepted, true );
	                
				}
	            ?>
                </div>

                <!--Proposal received-->
                <div class="block_r">
                    <h2><img src="<?php echo base_url(); ?>front/img/proposal-received-icon.png"> <?= $this->lang->line('proposal_received'); ?></h2>


                    <div class="row">
                        <div id="my-proposal-received" class="scroll-r">

                            <?php

								if ( !empty( $myproposal ) ) {
									$data_int[ 'myproposal' ] 	= $myproposal;
									echo $this->load->view( 'front/pages/proposal_received', $data_int, true );
								}
								
            					?>

                        </div>
                    </div>
                </div>
                <!--end-->

                <!--Proposal sent-->
                <div class="block_r">
                    <h2><img src="<?php echo base_url(); ?>front/img/proposal-sent-icon.png"> <?= $this->lang->line('proposal_sent'); ?></h2>
                    <div class="row">
                        <div class="scroll-r">
                            <?php
if (!empty($sentproposal)) {
                ?>
                            <div class="col-md-4">
                                <img src="<?php echo base_url() . userinfo($sentproposal[0]->respond_id)[0]->image ?>" class="condidate_pix">
                            </div>
                            <div class="col-md-8">

                                <div class="date margin"><i class="fa fa-calendar"></i> <?= $this->lang->line('date'); ?>: <span class="red"><?php echo date("d F, Y", strtotime($sentproposal[0]->request_sending_time)); ?></span></span></div>

                                <div class="name margin"><a href="<?php echo base_url() ?>viewprofile/<?php echo $sentproposal[0]->respond_id; ?>"><?php echo ucwords(userinfo($sentproposal[0]->respond_id)[0]->firstname); ?></a>, <?php echo calculateAge(user_basic_details($sentproposal[0]->respond_id)[0]->dob); ?>
                                    <button id="decline-sent-proposal" class="decline" style="display:none"><?= $this->lang->line('decline'); ?> </button>
                                    <input type="hidden" value="<?php echo $sentproposal[0]->id; ?>" id="sentproposalid">
                                </div>
                                <div class="margin"> &nbsp;</div>


                            </div>
                            <?php
}
            ?>
                        </div>
                    </div>
                </div>
                <!--end-->

                <!--Gift Receive-->
                <div class="block_r">
                    <h2><img src="<?php echo base_url(); ?>front/img/gift-icon.png"> <?= $this->lang->line('gift_received'); ?> (<span class="gift-recieved-cnt"><?php echo count($gift_recv) ?></span>)</h2>
                    <div class="row">
                        <div class="scroll-r view-received-gifts">
                            <?php
            							if (!empty($gift_recv)) {
                							for ($i = 0; $i < sizeof($gift_recv); $i++) {
                    				?>
                            <div id="gift_sec_<?php echo $gift_recv[$i]->rautoid; ?>" <?php if ($gift_recv[$i]->view_status == 1) {echo 'class="stats_view"';} else {echo 'class="stats_not_view"';}?>>
                                <div class="col-md-3 col-xs-3 col-sm-3" id="gift_img_<?php echo $gift_recv[$i]->rautoid; ?>" <?php if ($gift_recv[$i]->view_status == 1) {?>style="display:block;" <?php } else {?>style="display:none;<?php }?>">
                                    <?php if ($gift_recv[$i]->image == "") {?>
                                    <img src="<?php echo base_url() ?>dist/img/gifts.png" class="gifts_image">
                                    <?php } else { ?>
                                    <img src="<?php echo $gift_recv[$i]->image; ?>" class="gifts_image">
                                    <?php } ?>
                                </div>
                                <?php if ($gift_recv[$i]->view_status == 0) {?>
                                <div class="col-md-3 col-xs-3 col-sm-3 gift_img_<?php echo $gift_recv[$i]->rautoid; ?>"> <a href="javascript:void(0);" onclick="change_view_status('<?php echo $gift_recv[$i]->rautoid; ?>');"><?= $this->lang->line('view'); ?></a></div>
                                <?php }?>
                                <div class="col-md-7 col-xs-7 col-sm-7">
                                    <div class="name margin">
                                        <a href="<?php echo base_url() ?>viewprofile/<?php echo $gift_recv[$i]->sender_id; ?>"><?php if ($userdata->image == "") { ?>
                                            <img src="<?php echo base_url() ?>dist/img/male.png" class="gifts_profile_pic">
                                            <?php } else { ?>
                                            <img src="<?php echo base_url() . $gift_recv[$i]->uimage; ?>" class="gifts_profile_pic">
                                            <?php } ?>
                                            <?php echo ucwords($gift_recv[$i]->firstname); ?></a>
                                    </div>
                                    <div class="date margin"><i class="fa fa-calendar"></i> Date: <span class="red"><?php echo date("d F, Y", strtotime($gift_recv[$i]->date_created)); ?></span></div>

                                </div>
                                <div class="col-md-1 col-xs-1 col-sm-1"><i class="fa fa-trash" aria-hidden="true" onclick="delGiftSentReceived('<?php echo $gift_recv[$i]->rautoid; ?>');"></i></div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
										}
            						}
            						?>
                        </div>
                    </div>
                </div>
                <!--end-->
                <!--Gift Sent-->
                <div class="block_r">
                    <h2><img src="<?php echo base_url(); ?>front/img/gift-icon.png"> <?= $this->lang->line('gift_sent'); ?> (<span class="gift-sent-cnt"><?php echo count($gift_sent) ?></span>)</h2>
                    <div class="row">
                        <div class="scroll-r view-sent-gifts">
                            <?php
										if (!empty($gift_sent)) {
                							for ($i = 0; $i < sizeof($gift_sent); $i++) {
                    				?>
                            <div id="gift_sec_<?php echo $gift_sent[$i]->rautoid; ?>" <?php if ($gift_sent[$i]->view_status == 1) {echo 'class="stats_view"';} else {echo 'class="stats_not_view"';}?>>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                    <?php if ($gift_sent[$i]->image == "") { ?>
                                    <img src="<?php echo base_url() ?>dist/img/gifts.png" class="gifts_image">
                                    <?php  } else { ?>
                                    <img src="<?php echo $gift_sent[$i]->image; ?>" class="gifts_image">
                                    <?php } ?>
                                </div>
                                <div class="col-md-7 col-xs-7 col-sm-7">
                                    <div class="name margin"><a href="<?php echo base_url() ?>viewprofile/<?php echo $gift_sent[$i]->reciever_id; ?>">
                                            <?php
											if ($userdata->image == "") { ?>
                                            <img src="<?php echo base_url() ?>dist/img/male.png" class="gifts_profile_pic">
                                            <?php } else { ?>
                                            <img src="<?php echo base_url() . $gift_sent[$i]->uimage; ?>" class="gifts_profile_pic">
                                            <?php } ?>
                                            <?php echo ucwords($gift_sent[$i]->firstname); ?></a> </div>
                                    <div class="date margin"><i class="fa fa-calendar"></i> Date: <span class="red"><?php echo date("d F, Y", strtotime($gift_sent[$i]->date_created)); ?></span></div>
                                </div>
                                <div class="col-md-1 col-xs-1 col-sm-1"><i class="fa fa-trash" aria-hidden="true" onclick="delGiftSentReceived('<?php echo $gift_sent[$i]->rautoid; ?>');"></i></div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
											}
            							}
            							?>
                        </div>
                    </div>
                </div>
                <!--end-->

                <!--Wink Received-->
                <div class="block_r">
                    <h2><img src="<?php echo base_url(); ?>front/img/wink-receiv-icon.png"> <?= $this->lang->line('wink_received'); ?> (<span class="total-receivedwink-count"><?php echo count($wink_recv) ?></span>)</h2>
                    <div class="row">
                        <div class="scroll-r view-winkreceived-list">
                            <?php
										if (!empty($wink_recv)) {
										   for ($i = 0; $i < sizeof($wink_recv); $i++) {
									?>
                            <div id="gift_sec_<?php echo $wink_recv[$i]->rautoid; ?>" <?php if ($wink_recv[$i]->view_status == 1) {echo 'class="stats_view"';} else {echo 'class="stats_not_view"';}?>>
                                <div class="col-md-3 col-xs-3 col-sm-3" id="gift_img_<?php echo $wink_recv[$i]->rautoid; ?>" <?php if ($wink_recv[$i]->view_status == 1) {?>style="display:block;" <?php } else {?>style="display:none;<?php }?>">
                                    <?php
											if ($wink_recv[$i]->image == "") {
	                        				?><img src="<?php echo base_url() ?>dist/img/winks.png" class="gifts_image"><?php
											} else { ?>
                                    <img src="<?php echo $wink_recv[$i]->image; ?>" class="gifts_image">
                                    <?php } ?>
                                </div>
                                <?php if ($wink_recv[$i]->view_status == 0) {?>
                                <div class="col-md-3 col-xs-3 col-sm-3 gift_img_<?php echo $wink_recv[$i]->rautoid; ?>"><a href="javascript:void(0);" onclick="change_view_status('<?php echo $wink_recv[$i]->rautoid; ?>');"><?= $this->lang->line('view'); ?></a></div>
                                <?php }?>
                                <div class="col-md-7 col-xs-7 col-sm-7">
                                    <div class="name margin"><a href="<?php echo base_url() ?>viewprofile/<?php echo $wink_recv[$i]->sender_id; ?>">
                                            <?php if ($userdata->image == "") { ?>
                                            <img src="<?php echo base_url() ?>dist/img/male.png" class="gifts_profile_pic">
                                            <?php } else { ?>
                                            <img src="<?php echo base_url() . $wink_recv[$i]->uimage; ?>" class="gifts_profile_pic">
                                            <?php } ?>
                                            <?php echo ucwords($wink_recv[$i]->firstname); ?></a> </div>
                                    <div class="date margin"><i class="fa fa-calendar"></i> <?= $this->lang->line('date'); ?>: <span class="red"><?php echo date("d F, Y", strtotime($wink_recv[$i]->date_created)); ?></span></div>
                                </div>
                                <div class="col-md-1 col-xs-1 col-sm-1"><i class="fa fa-trash" aria-hidden="true" onclick="delGiftSentReceived('<?php echo $wink_recv[$i]->rautoid; ?>');"></i></div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
											}
									}
            						?>
                        </div>
                    </div>
                </div>
                <!--end-->


                <!--Wink Sent-->
                <div class="block_r">
                    <h2><img src="<?php echo base_url(); ?>front/img/wink-send-icon.png"> <?= $this->lang->line('wink_sent'); ?> (<span class="total-sentwink-count"><?php echo count($wink_sent) ?></span>)</h2>
                    <div class="row">
                        <div class="scroll-r view-winksent-list">
                            <?php
									if (!empty($wink_sent)) {
										for ($i = 0; $i < sizeof($wink_sent); $i++) {
                    				?>
                            <div id="gift_sec_<?php echo $wink_sent[$i]->rautoid; ?>" <?php if ($wink_sent[$i]->view_status == 1) {echo 'class="stats_view"';} else {echo 'class="stats_not_view"';}?>>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                    <?php
										if ($wink_sent[$i]->image == "") {?>
                                    <img src="<?php echo base_url() ?>dist/img/winks.png" class="gifts_image">
                                    <?php } else { ?>
                                    <img src="<?php echo $wink_sent[$i]->image; ?>" class="gifts_image">
                                    <?php } ?>
                                </div>
                                <div class="col-md-7 col-xs-7 col-sm-7">
                                    <div class="name margin"><a href="<?php echo base_url() ?>viewprofile/<?php echo $wink_sent[$i]->reciever_id; ?>">
                                            <?php if ($userdata->image == "") { ?>
                                            <img src="<?php echo base_url() ?>dist/img/male.png" class="gifts_profile_pic">
                                            <?php } else { ?>
                                            <img src="<?php echo base_url() . $wink_sent[$i]->uimage; ?>" class="gifts_profile_pic">
                                            <?php } ?>
                                            <?php echo ucwords($wink_sent[$i]->firstname); ?></a> </div>
                                    <div class="date margin"><i class="fa fa-calendar"></i> Date: <span class="red"><?php echo date("d F, Y", strtotime($wink_sent[$i]->date_created)); ?></span></div>
                                </div>
                                <div class="col-md-1 col-xs-1 col-sm-1"><i class="fa fa-trash" aria-hidden="true" onclick="delGiftSentReceived('<?php echo $wink_sent[$i]->rautoid; ?>');"></i></div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
									}
            					}
            					?>
                        </div>
                    </div>
                </div>
                <!--end-->
                <?php }?>
            <!-- </div> -->
            <?php }if ($devicestatus == 0) {
        ?>
            <!-- <div class="col-md-4"> -->
                <!--Who am I ?-->

                <div class="who_am_i">

                    <h2><?= $this->lang->line('who_am_i'); ?> ? <?php //echo $userdata->created_days ?></h2>
                    <?php $user_languages = json_decode($user_interest->languages);?>
                    <ul>



                        <?php if (!empty($userdata->city_name) || !empty($userdata->state_name)) {
            ?><li><span class="color-purple">Lives in:</span> <?php echo $userdata->city_name; ?>
                            , <?php echo $userdata->state_name; ?>
                            <?php
//echo '<pre>'; print_r($userlogs['Personal Description']['updated_on']);exit;
            if (isset($userlogs['Basic info'])) {
                echo '<span class="who_am_i_time"  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userlogs['Basic info']['updated_on']) . '</span>';
                }
            }
            ?></li>
                        <?php }?>



                        <?php if (!empty($userdata->user_profession)) {
            ?><li><span class="color-purple"><?= $this->lang->line('profession'); ?>ss:</span> <?php echo $userdata->user_profession; ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_profession_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>



                        <?php if (!empty($userdata->body_shape)) {
            ?><li><span class="color-purple"><?= $this->lang->line('body_type'); ?>:</span> <?php echo $userdata->body_shape ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->body_shape_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>




                        <?php if (!empty($userdata->height)) {
            ?><li><span class="color-purple"><?= $this->lang->line('height'); ?>:</span> <?php echo $userdata->height; ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->height_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>



                        <?php if (!empty($userdata->user_relationship)) {
            ?><li><span class="color-purple"><?= $this->lang->line('relationship'); ?>:</span> <?php echo $userdata->user_relationship; ?>
                            <?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_relationship_updated_on) . '</span>';
                }
            }
            ?>
                        </li><?php }?>


                        <?php if (!empty($userdata->user_religion)) {
            ?><li><span class="color-purple"><?= $this->lang->line('religion'); ?>:</span> <?php echo $userdata->user_religion ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_religion_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>



                        <?php if (!empty($userdata->user_race)) {
            ?><li><span class="color-purple"><?= $this->lang->line('race'); ?>:</span> <?php echo $userdata->user_race; ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_race_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>




                        <?php if (!empty($userdata->user_kids)) {
            ?><li><span class="color-purple"><?= $this->lang->line('have_kids'); ?>:</span> <?php echo $userdata->user_kids; ?>
                            <?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_kids_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>


                        <?php if (!empty($userdata->user_education)) {
            ?><li><span class="color-purple"><?= $this->lang->line('education'); ?>:</span> <?php echo $userdata->user_education; ?>
                            <?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->user_education_updated_on) . '</span>';
                }
            }
            ?>
                        </li><?php }?>
                        <?php if (!empty($user_languages)) {
            ?><li><span class="color-purple"><?= $this->lang->line('know'); ?>:</span> <?php echo implode(", ", $user_languages); ?>
                            <?php
if (isset($userlogs['Social Habits'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($user_interest->languages_updated_on) . '</span>';
                }
            }
            ?>
                        </li><?php }?>


                        <?php if (!empty($userdata->hair)) {
            ?><li><span class="color-purple"><?= $this->lang->line('hair'); ?>:</span> <?php echo $userdata->hair ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->hair_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>



                        <?php if (!empty($userdata->eye)) {
            ?><li><span class="color-purple"><?= $this->lang->line('eyes'); ?>:</span> <?php echo $userdata->eye ?><?php
if (isset($userlogs['Personal Description'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($userdata->eye_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>





                        <?php if (!empty($user_interest->drinking)) {
            ?><li><span class="color-purple"><?= $this->lang->line('drink'); ?>:</span> <?php echo $user_interest->drinking; ?><?php
if (isset($userlogs['Social Habits'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($user_interest->drinking_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>









                        <?php if (!empty($user_interest->smoking)) {
            ?><li><span class="color-purple"><?= $this->lang->line('smoke'); ?>:</span> <?php echo $user_interest->smoking; ?><?php
if (isset($userlogs['Social Habits'])) {
                echo '<span  class="who_am_i_time" style="font-size:11px;">&nbsp;';
                if (isset($userdata->created_days) && $userdata->created_days >= 30) {
                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . lastupdatetime($user_interest->smoking_updated_on) . '</span>';
                }
            }
            ?></li><?php }?>









                    </ul>

                </div>

                <!--end-->
            
            <?php }?>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--intrest-->
        <?php if ($userdata->face_verified == 1) {
        ?>
        <div class="row relatedimg">
            <div class="col-md-12">
                <h2><?= $this->lang->line('see_more_similar_profiles'); ?></h2>
            </div>
            <div class="col-md-12">
                <div id="owl-carousel" class="owl-carousel owl-theme">
                    <input type="hidden" name="searchlength" id="searchlength" value="<?php echo count($similarprofle) ?>">
                    <?php foreach ($similarprofle as $key => $value) {
            ?>



                    <?php //check 24 hour send praposal time over and account deactivte
            if (isset($value->request_status)) {

                if (time() - strtotime($value->request_status->request_sending_time) > 60 * 60 * 24) {
                    ?>
                    <script>
                        $.ajax({
                            url: "<?php echo base_url(); ?>/ajax/ajax/auto_decline_sent_proposal",
                            data: "proposal_id=" + <?php echo $value->request_status->id; ?>,
                            method: 'post',
                            success: function(result) {
                                if (result == "done") {
                                    //window.location.reload();
                                }
                            }
                        });

                    </script>
                    <?php //print "Older than 24h";
                } else {
                    ?>

                    <?php
//print "Newer than 24h";
                }
//exit;
            }
            ?>



                    <div class="item">
                        <div class="prod_box" style="padding:10px;">
                            <div class="head hidden-sm hidden-xs hidden-md hidden-lg" style="z-index:9">
                                <?php if (isset($similarprofle[$key]['request_status'])) {?>
                                <input type="hidden" name="oldtime<?php echo $key; ?>" id="oldtime<?php echo $key; ?>" value="<?php echo date('M d,Y H:i:s', strtotime($value['request_status']->request_sending_time)) ?>">
                                <div class="row">
                                    <i class="fa fa-clock-o"></i>
                                    <span class="red proposeDiv" id="show_timer_deactivate<?php echo $key; ?>"></span>

                                </div>
                                <span class="user_name"><?php echo ucwords($value['firstname']); ?></span>, <span class="user_age"><?php echo getage($value['dob']); ?></span>
                                <?php } else {?>
                                <a href="<?php echo base_url() ?>viewprofile/<?php echo $value['id'] ?>"> <span class="user_name"><?php echo ucwords($value['firstname']); ?></span>, <span class="user_age"><?php echo getage($value['dob']); ?></span></a>
                                <?php }?>


                                <?php if ($value['login_status']) {?>
                                <i class="fa fa-circle green"></i><br>
                                <?php } else {?>
                                <i class="fa fa-circle red"></i><br>
                                <?php }?>

                                <i class="fa fa-map-marker red"></i> <?php echo $value['country_name'] ?>
                            </div>
                            <?php if (isset($similarprofle[$key]['request_status'])) {?>
                            <img src="<?php echo base_url() ?>/<?php echo $value['image'] ?>" class="prod_img">
                            <?php } else {?>
                            <a href="<?php echo base_url() ?>viewprofile/<?php echo $value['id'] ?>"><img src="<?php echo base_url() ?>/<?php echo $value['image'] ?>" class="prod_img"></a>
                            <?php }?>
                            <div class="mobhead">

                                <?php
if ($value['login_status']) {
                $icn = '<i class="fa fa-circle green"></i>';
            } else {
                $icn = '<i class="fa fa-circle red"></i>';
            }
            ?>


                                <div class="row" style="padding:5px;">
                                    <?php if (isset($similarprofle[$key]['request_status'])) {?>
                                    <input type="hidden" name="oldtime<?php echo $key; ?>" id="oldtime<?php echo $key; ?>" value="<?php echo date('M d,Y H:i:s', strtotime($value['request_status']->request_sending_time)) ?>">


                                    <i class="fa fa-clock-o"></i>
                                    <span class="red proposeDiv" id="show_timer<?php echo $key; ?>"></span>

                                    <div>
                                        <?php echo $icn; ?> <span class="user_name"><?php echo ucwords($value['firstname']); ?></span>, <span class="user_age"><?php echo getage($value['dob']); ?></span>
                                    </div>



                                    <?php } else { ?>
                                    <?php echo $icn; ?>
                                    <a href="<?php echo base_url() ?>viewprofile/<?php echo $value['id'] ?>"><span class="user_name"> <?php echo ucwords($value['firstname']); ?></span>, <span class="user_age"><?php echo getage($value['dob']); ?></span></a>
                                    <?php } ?>
                                    <div class="loc_n">
                                        <i class="fa fa-map-marker red"></i> <?php echo $value['country_name']; ?></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <?php }?>

                </div>
            </div>
        </div>

        <?php }?>

    </div>
</section>
<?php
}
?>


<div id="giftModal" class="modal modal-wide fade modal_form" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn btn-default"  data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
            </div>
            <div class="modal-body" id="gift_modal">
            </div>
        </div>
    </div>
</div>


<div id="loginModel" class="modal fade modal_alert1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content pop_nm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    Login or Register
                </h4>
            </div>
            <div class="modal-body">
                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                <p><?= $this->lang->line('you_need_to_login'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
            </div>
        </div>

    </div>
</div>
<div id="idealMatchModel" class="modal fade modal_alert1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content pop_nm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?= $this->lang->line('criteria_are_not_matched'); ?></h4>
            </div>
            <div class="modal-body" id="showmessage">
                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                <?= $this->lang->line('your_ideal_match'); ?>
            </div>
            <div class="modal-footer">
            	<?= $this->lang->line('any_edit_in_your_profile'); ?>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
            </div>
        </div>

    </div>
</div>


<!--start video popup-->
<div id="video_chat" class="modal modal-wide fade in" tabindex="-1" aria-labelledby="modalLabel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
            </div>
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-body">
                    <!-- style="width: 400px; height: 400px;"-->
                    <div id="videos">
                        <div id="subscriber"></div>
                        <div id="publisher"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--end-->
<?php if (!$isMyProfile) {?>
<!-- breakup modal -->
<div id="sendproposalModal" class="modal modal-wide fade modal_alert1" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <?= $this->lang->line('send_proposal'); ?>
                <button type="button" class="proposalclose" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
            </div>
            <div class="modal-body">

                <div id="breakup_msg">
                    <p>
                    	<?= $this->lang->line('send_proposal_content'); ?> 
                    	<!-- <span class="pphone"><?= $this->lang->line('your_private_phone'); ?></span> -->
                    </p>
                    <!-- <p><span class="pphone"><?= $this->lang->line('warning'); ?>:</span> <?= $this->lang->line('once_you_submit'); ?> <span class="pphone"><?= $this->lang->line('disable_inactive'); ?></span> <?= $this->lang->line('for_up_24_hours'); ?></p> -->
                    <div class="commision"><span><strong><?= $this->lang->line('proposal_fees'); ?>:</strong></span><input type="text" value="$<?php echo $commessioncharge->proposal_sending_price; ?>" readonly></div>
                </div>
                <button name="cancel" id="cancel" data-dismiss="modal" class="btn btn-danger mr-5"><?= $this->lang->line('cancel'); ?></button>
                <button name="continue" id="continue" data-dismiss="modal" class="btn btn-success"><?= $this->lang->line('continue'); ?></button>

            </div>
        </div>
    </div>
</div>
<!--end-->
<?php }?>

<!-- breakup modal -->
<div id="breakupModal" class="modal modal-wide fade modal_alert" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content pop_nm ">
            <div class="modal-body ">
                <div id="breakup_msg">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    <h4><?= $this->lang->line('are_you_sure_want_to'); ?> <strong style="color: white;"><?= $this->lang->line('breakup'); ?></strong>?</h4>
                </div>
                <input type="button" name="breakup_staus" id="breakup_staus" value="Yes" class="btn btn-danger">
                <input type="button" name="breakup_no" data-dismiss="modal" value="No" class="btn btn-success">
            </div>
        </div>
    </div>
</div>
<!--end-->
<!-- breakup modal -->
<div id="blockModal" class="modal modal-wide fade modal_alert" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content pop_nm">
            <div class="modal-body">

                <div id="breakup_msg">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    <h4><?= $this->lang->line('are_you_sure_want_to'); ?> <strong style="color: white;"><?= $this->lang->line('block'); ?></strong>?</h4>
                </div>
                <input type="button" name="block_staus" id="block_staus" value="Yes" class="btn btn-danger">
                <input type="button" name="block_staus_no" data-dismiss="modal" value="No" class="btn btn-success">
            </div>
        </div>
    </div>
</div>
<!--end-->

<!-- breakup modal -->
<div id="declineModal" class="modal modal-wide fade modal_alert" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content pop_nm">
            <div class="modal-body">
                <div id="decline_msg">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    <h4><?= $this->lang->line('are_you_sure_want_to'); ?> <strong style="color: white;"><?= $this->lang->line('decline_proposal'); ?></strong>?</h4>
                </div>
                <input type="button" name="decline_staus" id="decline_staus" value="Yes" class="btn btn-danger">
                <input type="button" name="decline_staus_no" data-dismiss="modal" value="No" class="btn btn-success">
            </div>
        </div>
    </div>
</div>
<!--end-->

<!-- breakup modal -->
<div id="declineModal" class="modal modal-wide fade modal_alert" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content pop_nm">
            <div class="modal-body">
                <div id="decline_msg">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    <h4><?= $this->lang->line('are_you_sure_want_to'); ?> <strong style="color: white;"><?= $this->lang->line('decline_proposal'); ?></strong>?</h4>
                </div>
                <input type="button" name="decline_staus" id="decline_staus" value="Yes" class="btn btn-danger">
                <input type="button" name="decline_staus_no" data-dismiss="modal" value="No" class="btn btn-success">
            </div>
        </div>
    </div>
</div>
<!--end-->

<?php if ($isMyProfile) {?>
<!-- Receive proposal modal -->
<div id="receiveproposalModal" class="modal modal-wide fade modal_alert1" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content pop_nm">
            <div class="modal-header">
                <?= $this->lang->line('accepting_proposal'); ?>
                <button type="button" class="proposalclose" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
            </div>
            <div class="modal-body">

                <div id="breakup_msg">
                	<?= $this->lang->line('accepting_proposal_content'); ?>
                    <!-- <p> <strong><?= $this->lang->line('voice_chat_texting'); ?></strong> <?= $this->lang->line('and'); ?> <strong><?= $this->lang->line('video_chats'); ?></strong> <?= $this->lang->line('through_our_system'); ?> <div class="pphone"><?= $this->lang->line('your_private_phone'); ?></div>
                    </p>
                    <p><?= $this->lang->line('we_will_not_charge'); ?> </p>
                    <p><span class="pphone"><?= $this->lang->line('note'); ?>:</span> <?= $this->lang->line('if_you_ignore'); ?></p> -->
                    <div class="commision"> <span><strong><?= $this->lang->line('proposal_fees'); ?>:</strong></span><input type="text" value="$<?php echo $commessioncharge->proposal_receiving_price; ?>" readonly></div>
                </div>
                <button name="cancel" id="cancel" data-dismiss="modal" class="btn btn-danger mr-5"><?= $this->lang->line('cancel'); ?></button>
                <button name="continue" id="continue_accept" data-dismiss="modal" class="btn btn-success"><?= $this->lang->line('continue'); ?></button>
            </div>
        </div>
    </div>
</div>
<!--end-->
<?php }?>

<div id="modal_send_receive"></div>

<?php if (!empty($proposal_status) && $proposal_status[0]->user_id == $getSession["id"] && $proposal_status[0]->view_user_status == '0' && $proposal_status[0]->request_status!='block') {

    ?>

<div id="sendreceive" class="modal modal-wide fade modal_alert1" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content pop_nm">
            <div class="modal-header">
                <?php echo ucwords($proposal_status[0]->request_status); ?> <?= $this->lang->line('proposal'); ?>
                <button type="button" class="proposalclose" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
            </div>
            <div class="modal-body">

                <div id="breakup_msg">
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                    <p><?php if ($proposal_status[0]->request_status == 'accept') {?>
                        <?= $this->lang->line('congratulation_accepted'); ?>
                        <?php } else {?><?= $this->lang->line('sorry_your_proposal_is'); ?>
                        <?php echo ucwords($proposal_status[0]->request_status); ?>. <?= $this->lang->line('you_can_not_continue'); ?><?php }?>
                </div>
                </p>


            </div>
            <button name="cancel" id="proposalclose" data-dismiss="modal" class="btn btn-danger"><?= $this->lang->line('ok'); ?></button>

        </div>
    </div>
</div>
</div>
<?php }?>
<!-- Receive proposal modal -->

<!-- block and beackup -->
<?php //if (!empty($proposal_status) && $proposal_status[0]->block_breakup == $getSession["id"]) {

    ?>


<?php //}?>
<!-- end block breakup-->

<div id="profileactivateModal" class="modal modal-wide fade modal_alert1" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content pop_nm">
            <div class="modal-header">
                <?= $this->lang->line('activating_my_profile'); ?>
                <button type="button" class="proposalclose" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
            </div>
            <div class="modal-body">

                <div id="breakup_msg">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    <p><?= $this->lang->line('the_website_will_charge'); ?> <strong><?= $this->lang->line('reactivation_fees'); ?></strong> <?= $this->lang->line('every_time_ignore'); ?>
                    </p>

                    <div class="commision"> <span><strong><?= $this->lang->line('reactivation_fees'); ?></strong></span><input type="text" value="$<?php echo $commessioncharge->profile_activation_price; ?>" readonly></div>
                </div>
                <button name="cancel" id="cancel" data-dismiss="modal" class="btn btn-danger"><?= $this->lang->line('cancel'); ?></button>
                <button name="continue" id="continue" data-dismiss="modal" class="btn btn-success" onclick="cardmodel();"><?= $this->lang->line('continue'); ?></button>

            </div>
        </div>
    </div>
</div>
<!--end-->



<script src="<?php echo base_url() ?>front/gallery/dist/jquery.fancybox.min.js"></script>
<?php //trace($userinfo);echo $userinfo->userId .'=='.$getSession["id"];exit;
if ($userinfo->face_verified == 0 && $this->uri->segment(2) == $getSession["id"]) {
    echo '<script type="text/javascript">';
    echo 'alert("' . $this->lang->line('first_you_update') . '");';
    redirect('verify_picture');
    echo '</script>';} else {
    ;
}
?>
<script>
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
            return 'iOS';

        } else if (userAgent.match(/Android/i)) {

            return 'Android';
        } else {
            return 'unknown';
        }
    }

    //Show timer on similar profile
    <?php
	// echo "swati";
	// echo "<pre>";
	// print_r($data);
	// exit();
/*$date1 = new DateTime($value['request_status']->request_sending_time);
$now = new DateTime();*/
// strtotime($value['request_status']->request_sending_time)

$date1 					= date('Y-m-d H:i:s');
$now 					= date('Y-m-d H:i:s');


$difference_in_seconds 	= timeDifference($now, $date1);

?>
    var myIndex = 0;
    var remainingTime = <?php echo $difference_in_seconds; ?>;
    var timerKey = <?php echo $key; ?>;
    //clock(remainingTime,timerKey);
    carousel();

    var arraylength = $('#searchlength').val();

    // if( arraylength > 0 ) {
    // 	for (i = 0; i < arraylength; i++) {

    // 		var oldTime 			= $("#oldtime" + i).val();
    //         var countDownDate 		= new Date();
    //         countDownDate.setTime( new Date( oldTime ).getTime() + ( 24 * 60 * 60 * 1000 ) );
    //         var countDownDate 		= new Date( countDownDate ).getTime();

    //         if ( typeof oldTime != 'undefined' && oldTime ) {

    //         	var device 			= getMobileOperatingSystem();

    // 	if( device == 'unknown' ) {

    //  	var isPlatform 		= false;
    //        if ( navigator.appVersion.indexOf("Win") != -1 ) {

    //         	isPlatform 		=  true;
    //         	var now 		= new Date(Date.now()+(new Date().getTimezoneOffset()*60000)).getTime() + 160000;

    //      	} else {

    //      		var now 		= new Date(Date.now()+(new Date().getTimezoneOffset()*60000)).getTime();

    //      	}

    //    } else {

    // 	var now = new Date(Date.now()+(new Date().getTimezoneOffset()*60000)).getTime();

    //       }

    //          // Find the distance between now and the count down date
    //          var distance 		= countDownDate - now;
    //          var minutes 		= 0; seconds = 0;
    //          // Time calculations for days, hours, minutes and seconds
    //          var days 			= Math.floor(distance / (1000 * 60 * 60 * 24));
    //          var hours 			= Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    //          minutes 			= Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    //          seconds 			= Math.floor((distance % (1000 * 60)) / 1000);

    //          if(isPlatform && device =='unknown'){
    //          	if(minutes==58 && hours==23){
    //          		minutes = minutes+1;
    //          	}else{

    //          	}
    //          	if(minutes==57 && hours==23)
    //          		minutes = minutes+1;
    //          	if(seconds==42){
    //          		seconds = seconds+17;
    //          	}
    //          	if(seconds==43){
    //          		seconds = seconds+16;
    //          	}
    //          	if(seconds==44){
    //          		seconds = seconds+15;
    //          	}
    //          	if(seconds==45){
    //          		seconds = seconds+14;
    //          	}
    //          }else{
    //          	if(seconds==56){
    //              	seconds = seconds+3;
    //              }
    //              if(seconds==57)
    //          		seconds = seconds+2;
    //          }
    //          if (hours <= 9) {
    //              hours = '0' + hours;
    //          }
    //          if (minutes <= 9) {
    //              minutes = '0' + minutes;
    //          }else{

    //          }
    //          if (seconds <= 9) {
    //              seconds = '0' + seconds;
    //          }

    //          //console.log( '#show_timer' + i + '-------' + hours + " : " + minutes + " : " + seconds );
    //          $('#show_timer' + i).html(hours + " : " + minutes + " : " + seconds);
    //          $('#show_timer_deactivate' + i).html(hours + " : " + minutes + " : " + seconds);


    //         } 

    // 	}
    // }

    function carousel() {
        var i;
        var arraylength = $('#searchlength').val();

        for (i = 0; i < arraylength; i++) {
            var oldTime = $("#oldtime" + i).val();
            var countDownDate = new Date();
            countDownDate.setTime(new Date(oldTime).getTime() + (24 * 60 * 60 * 1000));
            var countDownDate = new Date(countDownDate).getTime();
            if (typeof oldTime != 'undefined' && oldTime) {
                // Get todays date and time
                //var now = new Date().getTime();
                //var now = new Date(Date.now()+(new Date().getTimezoneOffset()*60000)).getTime();


                var device = getMobileOperatingSystem();

                if (device == 'unknown') {

                    var isPlatform = false;
                    if (navigator.appVersion.indexOf("Win") != -1) {

                        isPlatform = true;
                        var now = new Date(Date.now() + (new Date().getTimezoneOffset() * 60000)).getTime() + 160000;

                    } else {

                        var now = new Date(Date.now() + (new Date().getTimezoneOffset() * 60000)).getTime();

                    }

                } else {

                    var now = new Date(Date.now() + (new Date().getTimezoneOffset() * 60000)).getTime();

                }


                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                var minutes = 0;
                seconds = 0;
                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                seconds = Math.floor((distance % (1000 * 60)) / 1000);

                if (isPlatform && device == 'unknown') {
                    if (minutes == 58 && hours == 23) {
                        minutes = minutes + 1;
                    } else {

                    }
                    if (minutes == 57 && hours == 23)
                        minutes = minutes + 1;
                    if (seconds == 42) {
                        seconds = seconds + 17;
                    }
                    if (seconds == 43) {
                        seconds = seconds + 16;
                    }
                    if (seconds == 44) {
                        seconds = seconds + 15;
                    }
                    if (seconds == 45) {
                        seconds = seconds + 14;
                    }
                } else {
                    if (seconds == 56) {
                        seconds = seconds + 3;
                    }
                    if (seconds == 57)
                        seconds = seconds + 2;
                }
                if (hours <= 9) {
                    hours = '0' + hours;
                }
                if (minutes <= 9) {
                    minutes = '0' + minutes;
                } else {

                }
                if (seconds <= 9) {
                    seconds = '0' + seconds;
                }

                $('#show_timer' + i).html(hours + " : " + minutes + " : " + seconds);
                $('#show_timer_deactivate' + i).html(hours + " : " + minutes + " : " + seconds);


            }
        }
        myIndex++;
        if (myIndex > arraylength) {
            myIndex = 1
        }

        setTimeout(carousel, 1000); // Change image every 2 seconds
        for (i = 0; i < arraylength; i++) {
            var oldTime = $("#oldtime" + i).val();
            var countDownDate = new Date();
            countDownDate.setTime(new Date(oldTime).getTime() + (24 * 60 * 60 * 1000));
            var countDownDate = new Date(countDownDate).getTime();
            if (typeof oldTime != 'undefined' && oldTime) {
                // Get todays date and time
                //var now = new Date().getTime();
                var now = new Date(Date.now() + (new Date().getTimezoneOffset() * 60000)).getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                var minutes = 0;
                seconds = 0;
                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                seconds = Math.floor((distance % (1000 * 60)) / 1000);

                if (isPlatform && device == 'unknown') {
                    if (minutes == 58 && hours == 23) {
                        minutes = minutes + 1;
                    } else {

                    }
                    if (minutes == 57 && hours == 23)
                        minutes = minutes + 1;
                    if (seconds == 42) {
                        seconds = seconds + 17;
                    }
                    if (seconds == 43) {
                        seconds = seconds + 16;
                    }
                    if (seconds == 44) {
                        seconds = seconds + 15;
                    }
                    if (seconds == 45) {
                        seconds = seconds + 14;
                    }
                } else {
                    if (seconds == 56) {
                        seconds = seconds + 3;
                    }
                    if (seconds == 57)
                        seconds = seconds + 2;
                }
                if (hours <= 9) {
                    hours = '0' + hours;
                }
                if (minutes <= 9) {
                    minutes = '0' + minutes;
                } else {

                }
                if (seconds <= 9) {
                    seconds = '0' + seconds;
                }

                $('#show_timer' + i).html(hours + " : " + minutes + " : " + seconds);
                $('#show_timer_deactivate' + i).html(hours + " : " + minutes + " : " + seconds);

            }
        }
    }


    function clock(c, k) {
        myTimer = setInterval(myClock, 1000);
        //var c = 3600; //Initially set to 1 hour


        function myClock() {
            --c
            var seconds = c % 60; // Seconds that cannot be written in minutes
            var secondsInMinutes = (c - seconds) / 60; // Gives the seconds that COULD be given in minutes
            var minutes = secondsInMinutes % 60; // Minutes that cannot be written in hours
            var hours = (secondsInMinutes - minutes) / 60;
            $("#show_timer" + k).html(hours + ":" + minutes + ":" + seconds);
            if (c == 0) {
                clearInterval(myTimer);
            }
        }
    }

    <?php
	//PENDING STATUS CHECK 
	if( $request_pending ) { ?>
    proposeDivTime();
    <?php } ?>

    function proposeDivTime() {
        var oldTime = $("#oldtime").val();
        console.log('oldTime', oldTime);
        var countDownDate = new Date();
        countDownDate.setTime(new Date(oldTime).getTime() + (24 * 60 * 60 * 1000));
        console.log('countDownDate', countDownDate);
        var countDownDate = new Date(countDownDate).getTime();
        console.log('countDownDate1', countDownDate);
        console.log('timer');
        // Update the count down every 1 second
        if (typeof oldTime != 'undefined' && oldTime) {
            var x = setInterval(function() {

                // Get todays date and time
                // var now = new Date().getTime();
                var device = getMobileOperatingSystem();
                // alert(device);
                if (device == 'unknown') {
                    var isPlatform = false;
                    if (navigator.appVersion.indexOf("Win") != -1) {
                        isPlatform = true;
                        var now = new Date(Date.now() + (new Date().getTimezoneOffset() * 60000)).getTime() + 160000;
                    } else {
                        var now = new Date(Date.now() + (new Date().getTimezoneOffset() * 60000)).getTime();
                    }

                    /*if (navigator.appVersion.indexOf("Mac") != -1) Name =
                      "MacOS";
                    if (navigator.appVersion.indexOf("X11") != -1) Name =
                      "UNIX OS";
                    if (navigator.appVersion.indexOf("Linux") != -1) Name =
                      "Linux OS";*/

                    //var now = new Date(Date.now()+(new Date().getTimezoneOffset()*60000)).getTime();
                } else {
                    var now = new Date(Date.now() + (new Date().getTimezoneOffset() * 60000)).getTime();

                }

                console.log(now);

                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                var minutes = 0;
                seconds = 0;
                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                seconds = Math.floor((distance % (1000 * 60)) / 1000);

                if (isPlatform && device == 'unknown') {
                    if (minutes == 58 && hours == 23) {
                        minutes = minutes + 1;
                    } else {

                    }
                    if (minutes == 57 && hours == 23)
                        minutes = minutes + 1;
                    if (seconds == 42) {
                        seconds = seconds + 17;
                    }
                    if (seconds == 43) {
                        seconds = seconds + 16;
                    }
                    if (seconds == 44) {
                        seconds = seconds + 15;
                    }
                    if (seconds == 45) {
                        seconds = seconds + 14;
                    }
                } else {
                    if (seconds == 56) {
                        seconds = seconds + 3;
                    }
                    if (seconds == 57)
                        seconds = seconds + 2;
                }
                if (hours <= 9) {
                    hours = '0' + hours;
                }
                if (minutes <= 9) {
                    minutes = '0' + minutes;
                } else {

                }
                if (seconds <= 9) {
                    seconds = '0' + seconds;
                }

                $('.proposeDiv').html(hours + " : " + minutes + " : " + seconds);

                // If the count down is finished, write some text
                if (distance < 0) {
                    var proposalId = $("#sentproposalid").val();
                    $.ajax({
                        url: "<?php echo base_url(); ?>/ajax/ajax/auto_decline_sent_proposal",
                        data: "proposal_id=" + proposalId,
                        method: 'post',
                        success: function(result) {
                            if (result == "done") {
                                //window.location.reload();
                            }
                        }
                    });
                }
            }, 1000);
        }
    }


    $(document).ready(function() {

        $( '.amazingcarousel-image div' ).remove();

        setInterval(function() {
            setUsersLoggedIn();

            check_current_request_status();

        }, 5000);

        function check_current_request_status() {

            var profile_id = '<?= $this->uri->segment( 2 ); ?>';
            $.ajax({
                url: "<?php echo base_url(); ?>ajax/ajax/check_current_request_status",
                data: "id=" + <?php echo $this->session->userdata["id"]; ?> + '&profile_id=' + profile_id,
                dataType: "json",
                method: 'post',
                success: function(result) {
                    console.log(result);
                    console.log("proposallength:" + $('#proposeDiv').html().length);
                    if (result.timer == "1") {
                        //IF STATUS IS PENDING - TIMER
                        if ($('#proposeDiv').html().length == 0) {
                            $("#tr").show();
                            proposeDivTime();
                        }
                    } else {
                        $("#tr").hide();
                        $('#proposeDiv').html('');
                        $("#hidetimer").hide();
                        $("#current_request_status").text(result.request_status);
                    }

                    //PROPOSE ACCEPT ICON
                    $('#propose_accept_icon').html(result.propose_accept_icon);

                    // console.log(result);
                    if (result.status == "done") {

                        if (result.view_user_status == 0 && result.modal_send_receive != "") {

                            $('#modal_send_receive').html(result.modal_send_receive);
                            setTimeout(function() {
                                $("#sendreceive_modal").modal();
                            }, 1000);
                        }

                    } else {

                        $("#current_request_status").text(result.request_status);

                        if (result.view_user_status == 0 && result.modal_send_receive != "") {

                            $('#modal_send_receive').html(result.modal_send_receive);
                            setTimeout(function() {
                                $("#sendreceive_modal").modal();
                            }, 1000);
                        }
                    }
                }
            });
        }

        $(".edit_section").click(function() {
            //$("#myModal").modal();
            //alert($(this).attr('id'));
            var sectionName = $(this).attr('id');
            $.ajax({
                url: "<?php echo base_url(); ?>/ajax/ajax/getform",
                data: "form_name=" + sectionName + "&reciever_id=<?php echo $this->uri->segment(2); ?>",
                method: 'post',
                success: function(result) {
                    //console.log(result);
                    $("#gift_modal").html(result);
                    // $( "#card1" ).clone().appendTo( ".clone-div" );
                    // $('[data-mask]').inputmask()
                    $("#giftModal").modal();
                }
            });
        });

        <?php if (!$isMyProfile) {?>

        $("#propose_section").click(function() {
            $("#sendproposalModal").modal();
        });

        $('body').on('click', '#continue', function() {
            $("#propose_section").attr("disabled", true);
            //alert("Pro");
            $.ajax({
                url: "<?php echo base_url(); ?>/ajax/ajax/propose",
                data: "reciever_id=<?php echo $this->uri->segment(2); ?>",
                method: 'post',
                success: function(result) {
                    //console.log(result);
                    //alert('ys');
                    $("#propose_section").attr("disabled", false);
                    if (result == 'not-done') {
                        alert("Already this person requested for proposal");
                    }
                    //window.location.reload();

                    var data_arr = result.split("|");
                    // alert(data_arr);
                    //alert("Your Payment for proposal Accepted has been completed successfully.");
                    //window.location.reload();
                    $('#sender_id').val(data_arr[1]);
                    $('#respond_id').val(data_arr[2]);
                    $('#proposal_id').val(data_arr[3]);
                    $('#action').val(data_arr[4]);
                    $("#cardModal").modal();
                    // obj = JSON.parse(result);
                    // $("#oldtime").val(obj.oldtime);
                    // $("#tr").css("display", "block");
                    // $('.proposeDiv').html(obj.newtime);
                    //$('#propose_section').remove();
                    //window.location.reload();
                }
            });
        });
        <?php }?>


        lessString("#dots", "#more", "#myBtn");
        lessString("#dots2", "#more2", "#myBtn2");

        function lessString(dots, more, btn) {
            $(btn).click(function() {
                var dotscss = $(dots).css("display");
                if (dotscss == "none") {
                    $(dots).css("display", "inline");
                    $(btn).text("Read More");
                    $(more).css("display", "none");
                } else {
                    $(dots).css("display", "none");
                    $(btn).text("Read Less");
                    $(more).css("display", "inline");
                }
            });
        }


        function setUsersLoggedIn() {
            if (typeof localStorage.getItem("UsersLoggedIn") !== "undefined") {
                var ls = JSON.parse(localStorage.getItem("UsersLoggedIn"));
                var x = ls.filter((o) => {
                    return o.id == <?php echo $this->uri->segment(2); ?>;
                });

                if (x.length == 0) {
                    $(".online-userr").css("display", "none");
                    $(".offline-userr").css("display", "inline-block");
                } else {
                    $(".online-userr").css("display", "inline-block");
                    $(".offline-userr").css("display", "none");
                }
            }
        }


    });
    $(function() {
        //Initialize Select2 Elements
        $('.select2').select2();

        var $select2 = $('.select2').select2({
            containerCssClass: "wrap"
        })
    });


    $(document).on('click', '.panel-heading span.icon_minim', function(e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.removeClass('fa-minus').addClass('fa-plus');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.removeClass('fa-plus').addClass('fa-minus');
        }
    });
    $(document).on('focus', '.panel-footer input.chat_input', function(e) {
        var $this = $(this);
        if ($('#minim_chat_window').hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideDown();
            $('#minim_chat_window').removeClass('panel-collapsed');
            $('#minim_chat_window').removeClass('fa-plus').addClass('fa-minus');
        }
    });
    $(document).on('click', '#new_chat', function(e) {
        var size = $(".chat-window:last-child").css("margin-left");
        size_total = parseInt(size) + 400;
        var clone = $("#chat_window_1").clone().appendTo(".container");
        clone.css("margin-left", size_total);
    });
    $(document).on('click', '.icon_close', function(e) {
        //$(this).parent().parent().parent().parent().remove();
        $("#chat_window_1").css("display", "none");
        //$("#chat_window_1").remove();
    });

    $(".broadcastmsg").click(function() {
        $(".userchat").toggle();
        //$('#all-chat').scrollTop(1000000);
        var objDiv = document.getElementById("all-chat");
        objDiv.scrollTop = objDiv.scrollHeight;
    });


    $("#btn-chat").click(function() {
        var msg = $(".broadcast-msg").val();
        var recv = <?php echo $this->uri->segment(2); ?>;
        if (msg == "") {
            alert("<?= $this->lang->line('please_choose_a_message'); ?>");
        } else {
            $.ajax({
                url: "<?php echo base_url(); ?>ajax/ajax/userbroadcast",
                data: "template_id=" + msg + "&recv=" + recv,
                method: 'post',
                success: function(res) {
                    //console.log(res);
                    if (res == 200) {
                        $("#all-chat").load(location.href + " #all-chat");
                        $.wait(function() {
                            var objDiv = document.getElementById("all-chat");
                            objDiv.scrollTop = objDiv.scrollHeight;
                        }, 2);

                        //$("#all-chat").scroll($("#all-chat").height() + " px");

                    }
                }

            });
        }

    });

    var ajax_call = function() {
        var recv = <?php echo $this->uri->segment(2); ?>;
        $.ajax({
            url: "<?php echo base_url(); ?>ajax/ajax/getbroadcastMessages",
            data: "receiver_id=" + recv,
            method: 'post',
            dataType: 'json',
            success: function(res) {
                if (res.length > 0) {
                    var j = 0;
                    jQuery.each(res, function(i, val) {
                        if (val.user_id == "<?php echo $this->uri->segment(2) ?>") {
                            $("#all-chat").append('<div class="row msg_container base_receive" id="other"><div class="col-md-2 col-xs-2 avatar">' +
                                '<img src="<?php echo base_url() . userinfo($this->uri->segment(2))[0]->image ?>" class=" img-responsive "></div>' +
                                '<div class="col-md-10 col-xs-10"><div class="messages msg_receive">' +
                                '<p>' + val.message + '</p>' +
                                '<time datetime="2009-11-13T20:00">' + val.updated_on + '</time></div></div></div>');
                            updatechatStatus('<?php echo $this->uri->segment(2) ?>', val.id);

                            $("#all-chat").load(location.href + " #all-chat");
                            $.wait(function() {
                                var objDiv = document.getElementById("all-chat");
                                objDiv.scrollTop = objDiv.scrollHeight;
                            }, 2);
                            //$("#all-chat").scroll($("#all-chat").height() + " px");

                        }
                        //                                                            if (val.user_id == "<?php echo $profileid; ?>") {
                        //                                                                alert('chauhan');
                        //                                                                $("#all-chat").append('<div class="row msg_container base_sent" id="self"><div class="col-md-10 col-xs-10">'
                        //                                                                        + '<div class="messages msg_sent">'
                        //                                                                        + '<p>'+val.message +'</p>'
                        //                                                                        + '<time datetime="2009-11-13T20:00">' + val.updated_on + '</time></div></div>'
                        //                                                                        + '<div class="col-md-2 col-xs-2 avatar"><img src="<?php echo base_url() . userinfo($profileid)[0]->image ?>" class=" img-responsive ">'
                        //                                                                        + '</div></div>');
                        //
                        //                                                            }


                    });

                }
            }

        });
    };

    function updatechatStatus(respondId, msgId) {
        $.ajax({
            url: "<?php echo base_url(); ?>ajax/ajax/updatebroadcastMessages",
            data: "receiver_id=" + respondId + "&msg_id=" + msgId,
            method: 'post',
            dataType: 'json',
            success: function(res) {}
        });
    }

    var interval = 1000;

    //setInterval(ajax_call, interval);//remove it later

    function addHtml() {
    	$(".modal-title").html("<?= $this->lang->line('criteria_are_not_matched'); ?>");
        $(".modal-body").html("<?= $this->lang->line('not_matched_cont'); ?>");
    }

    function unblockUser(proposalId, userId, respondId) {
        $("#unblock_section").attr("disabled", true);
        $.ajax({
            url: "<?php echo base_url(); ?>/ajax/ajax/unblockUser",
            data: "proposal_id=" + proposalId + "&sender_id=" + userId + "&respond_id=" + respondId,
            method: 'post',
            success: function(result) {
                $("#unblock_section").attr("disabled", false);
                window.location.reload();
                if (result == "done") {
                    window.location.reload();
                }
            }
        });
    }

    function showMessage() {
        $("#showmessage").html("<p><?= $this->lang->line('your_profile_not_verfied'); ?></p>");
        //step jump
        //$("#showmessage").attr("disabled", true);
        setTimeout(function() {
            window.location.replace("<?php echo base_url() ?>verify_picture");
        }, 5000);
        //end
        //window.location.href = "<?php echo base_url() ?>verify_picture";
        // $("#cardModal").modal();
    }

    // setInterval(video_call, 5000);
    // function video_call() {
    // $("#video_chat").modal();
    // }

    function deleteAllCookies() {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    $(".close").click(function() {
        pauseAudio();
        setCookie("videopopup", 1, 365);
        $.ajax({
            url: "<?php echo base_url(); ?>/ajax/ajax/delVideoNotifications",
            data: "userid=" + '<?php echo $userdata->userId; ?>',
            method: 'post',
            success: function(result) {
                $('#showvideoicon').hide();
                $('#showvideoiconmob').hide();
                if (result) {
                    window.location.href = "<?php echo base_url() ?>myprofile";
                }
            }
        });

    });
    $(".videocalling").click(function() {
        setCookie("videopopup", '', '');
    });


    $("#decline-sent-proposal").click(function() {
        $("#decline-sent-proposal").attr("disabled", true);
        //alert();
        var proposalId = $("#sentproposalid").val();
        $.ajax({
            url: "<?php echo base_url(); ?>/ajax/ajax/decline_sent_proposal",
            data: "proposal_id=" + proposalId,
            method: 'post',
            success: function(result) {
                $("#decline-sent-proposal").attr("disabled", false);
                window.location.reload();
                console.log(result);
                if (result == "done") {
                    window.location.reload();
                }
            }
        });
    });

    $("#accept-recv-proposal").click(function() {
        $("#receiveproposalModal").modal();
    });

    $("#continue_accept").click(function() {

        var proposalId = $("#recvproposalid").val();

        $("#accept-recv-proposal").attr("disabled", true);
        $.ajax({
            url: "<?php echo base_url(); ?>ajax/ajax/accept_proposal",
            data: "proposal_id=" + proposalId + "&sender_id=<?php echo $myproposal[0]->user_id; ?>&respond_id=<?php echo $myproposal[0]->respond_id; ?>",
            method: 'post',
            success: function(result) {
                $("#accept-recv-proposal").attr("disabled", false);
                console.log(result);
                result = $.trim( result );
                var data_arr = result.split("|");
                if (data_arr[0] == "done") {
                    //alert("Your Payment for proposal Accepted has been completed successfully.");
                    //window.location.reload();
                    $('#sender_id').val(data_arr[1]);
                    $('#respond_id').val(data_arr[2]);
                    $('#proposal_id').val(data_arr[3]);
                    $('#action').val(data_arr[4]);
                    $("#cardModal").modal();
                } else {
                    alert(result);
                }
            }
        });
    });

    $("#decline-recv-proposal").click(function() {
        $("#declineModal").modal();
    });

    $("#decline_staus").click(function() {
        $("#decline-recv-proposal").attr("disabled", true);
        var proposalId = $("#recvproposalid").val();
        $.ajax({
            url: "<?php echo base_url(); ?>/ajax/ajax/reject_proposal",
            data: "proposal_id=" + proposalId + "&sender_id=<?php echo $myproposal[0]->user_id; ?>&respond_id=<?php echo $myproposal[0]->respond_id; ?>",
            method: 'post',
            success: function(result) {
                console.log(result);
                window.location.reload();
                if (result == "done") {
                    $("#decline-recv-proposal").attr("disabled", false);
                    window.location.reload();
                }
            }
        });
    });

    $("#block-recv-proposal").click(function() {
        $("#blockModal").modal();
        $("#block_staus").addClass('block-recv-sec');
        $('.block-recv-sec').removeAttr('id');
    });


    $(document).on("click", ".block-recv-sec", function() {
        $("#block-recv-proposal").attr("disabled", true);
        var proposalId = $("#recvproposalid").val();
        $.ajax({
            url: "<?php echo base_url(); ?>/ajax/ajax/block_proposal",
            data: "proposal_id=" + proposalId + "&sender_id=<?php echo $myproposal[0]->user_id; ?>&respond_id=<?php echo $myproposal[0]->respond_id; ?>&block_by=<?php echo $userdata->userId; ?>",
            method: 'post',
            success: function(result) {
                $("#block-recv-proposal").attr("disabled", false);
                //window.location.reload();
                if (result == "done") {
                    window.location.reload();
                }
            }
        });
    });

    //afetr Acceptance the Request
    $("#breakup").click(function() {

        $("#breakupModal").modal();
    });

    $("#breakup_staus").click(function() {
        $("#breakup").attr("disabled", true);
        var proposalId = $("#acceptedproposal").val();
        $.ajax({
            url: "<?php echo base_url(); ?>ajax/ajax/breakup",
            data: "proposal_id=" + proposalId + "&sender_id=<?php echo $accepted[0]->user_id; ?>&respond_id=<?php echo $accepted[0]->respond_id; ?>&block_by=<?php echo $userdata->userId; ?>",
            method: 'post',
            success: function(result) {
                $(".modal-backdrop").hide();
                $("#breakupModal").hide();
                $("#breakup").attr("disabled", false);
                window.location.reload();
                if (result == "done") {
                    window.location.reload();
                }
            }
        });
    });

    $("#block").click(function() {
        $("#blockModal").modal();
    });

    $(document).on("click", "#block_staus", function() {
        $("#block").attr("disabled", true);
        var proposalId = $("#acceptedproposal").val();
        var respondId = "<?php echo $accepted[0]->respond_id; ?>";
        var senderId = "<?php echo $accepted[0]->user_id; ?>";
        $.ajax({
            url: "<?php echo base_url(); ?>/ajax/ajax/block_proposal",
            data: "proposal_id=" + proposalId + "&sender_id=" + senderId + "&respond_id=" + respondId + "&block_by=<?php echo $userdata->userId; ?>",
            method: 'post',
            success: function(result) {
                $("#block").attr("disabled", false);
                console.log(result);
                //window.location.reload();
                if (result == "done") {
                    window.location.reload();
                }
            }
        });
    });

    function activateUserPopup() {
        $("#profileactivateModal").modal();
    }

    function cardmodel() {
        $("#activateBtn").attr("disabled", false);
        $("#cardModal").modal();

    }

    function activateUser() {
        $("#activateBtn").attr("disabled", true);
        $.ajax({
            url: "<?php echo base_url(); ?>front/Frontend/activateuser",
            method: 'post',
            dataType: 'json',
            success: function(result) {
                $("#activateBtn").attr("disabled", false);
                if (result.status == "SUCCESS") {
                    window.location.reload();
                }
            }
        });

    }

    function delGiftSentReceived(id) {
        var res = confirm("<?= $this->lang->line('do_you_want_to_delete'); ?>");
        if (res == true) {
            $.ajax({
                url: "<?php echo base_url(); ?>front/Frontend/delsentreceivedgift",
                method: 'post',
                data: "id=" + id,
                success: function(result) {
                    if (result) {
                        jQuery('#gift_sec_' + id).hide();
                    }
                }
            });
        }
    }


    function change_view_status(id) {
        //var res = confirm("Do you want to delete this record!");

        $.ajax({
            url: "<?php echo base_url(); ?>front/Frontend/gift_received",
            method: 'post',
            data: "id=" + id,
            success: function(result) {
                //console.log(result);
                if (result) {
                    jQuery('#gift_img_' + id).show();
                    jQuery('.gift_img_' + id).hide();
                    jQuery('#gift_sec_' + id).css("background-color", "#ffffcc");
                    //alert("view ")
                }
            }
        });

    }

</script>
<link rel="stylesheet" href="<?php echo base_url() ?>front/css/owl.theme.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>front/css/owl.carousel.css" />
<script src="<?php echo base_url() ?>front/js/owl.carousel.min.js"></script>
<script>
    $(document).ready(function() {
        $("#owl-carousel").owlCarousel({
            itemsCustom: [
                [0, 3],
                [375, 3],
                [600, 3],
                [992, 4],
                [1200, 6]
            ],

            autoPlay: false,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            navigation: false,
            pagination: false,

        });


    });

    $(".show-more a").on("click", function() {
        var $this = $(this);
        var $content = $this.parent().prev("div.content");
        var linkText = $this.text().toUpperCase();

        if (linkText === "... READ MORE") {
            linkText = "read less";
            $content.switchClass("hideContent", "showContent", 200);
            $(".show-more").css({
                "float": "none"
            });
        } else {
            linkText = "... read more";
            $content.switchClass("showContent", "hideContent", 200);
            $(".show-more").css({
                "float": "right"
            });
        };

        $this.text(linkText);
    });

</script>
<?php
if ($_REQUEST['chat'] == 'video' && $_REQUEST['device'] != 'mobile') {
    if ($_COOKIE["videopopup"] == '') {
        ?>
<script type="text/javascript">
    $(window).on('load', function() {
        $("#video_chat").modal({
            backdrop: 'static',
            keyboard: false
        });
        playAudio();
    });

</script>
<?php }?>
<?php } else if ($_REQUEST['device'] == 'mobile') {

    $_COOKIE["videopopup"] = '';
    ?>
<?php
if ($_COOKIE["videopopup"] == '') {
        ?>
<script type="text/javascript">
    $(window).on('load', function() {
        $("#video_chat").modal({
            backdrop: 'static',
            keyboard: false
        });
        playAudio();
    });

</script>
<?php }?>
<?php }?>

<?php //added subash
//trace($proposal_status);

if (!empty($proposal_status) && $proposal_status[0]->user_id == $getSession["id"] && $proposal_status[0]->view_status == '1') {
    ?><script>
    //$("#sendreceive").modal();

</script>
<?php

}?>

<?php //added subash
//trace($proposal_status);

// if (!empty($proposal_status) && $proposal_status[0]->block_breakup == $getSession["id"]) {
    ?>
<!-- <script> -->
<!-- $("#blockbreakup").modal(); -->
<!-- </script> -->
<?php

// }?>






<?php

function timeDifference($start, $end)
{

    $start_ts = strtotime($start);
    $end_ts = strtotime($end);

    if ($start_ts > $end_ts) {
        $diff = $start_ts - $end_ts;
    } else {
        $diff = $end_ts - $start_ts;
    }
    return $diff;

}
?>
<!-- tokbox call code -->
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script>
    var token_result;
    $.ajax({
        url: "<?php echo base_url(); ?>/ajax/ajax/get_call_tokbox",
        data: "user_id=" + <?php echo $view_id; ?>,
        async: false,
        method: 'post',
        success: function(result) {
            token_result = result;
        }
    });
    // console.log(token_result, "this is result");
    var token_session_value = token_result;
    var parts = token_session_value.split('&', 2);
    var apiKey = '<?php echo $apiKey ?>';
    var sessionId = parts[1];
    var tokenId = parts[0];



    $(function() {
        $('#vid-btn').on('click', function() {
            if ($(this).hasClass('fa-video')) {
                $(this).removeClass('fa-video').addClass('fa-video-slash');
                hide_video();
            } else if ($(this).hasClass('fa-video-slash')) {
                $(this).removeClass('fa-video-slash').addClass('fa-video');
                show_video();
            }
        });
    })
    $(function() {
        $('#vol-btn').on('click', function() {
            if ($(this).hasClass('fa-microphone')) {
                $(this).removeClass('fa-microphone').addClass('fa-microphone-slash');
                mute_audio();

            } else if ($(this).hasClass('fa-microphone-slash')) {
                $(this).removeClass('fa-microphone-slash').addClass('fa-microphone');
                unmute_audio();
            }
        });
    })

</script>
<script type="text/javascript" src="<?php echo base_url() ?>application/views/front/pages/newapp/js/viewprofile.js"></script>


<!-- IF ACCOUNT IS INACTIVE -->
<?php 
if ($deactive && $this->uri->segment(2) == $userinfo->userId) { ?>

<!-- disabled pop up -->
<div id="disabled" class="modal modal-wide fade modal_alert1"  tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content pop_nm">
            <div class="modal-header">
                <span class="model_request_status" style="text-transform: capitalize;"><?= $this->lang->line('disabled_exclamation'); ?>
                <!-- <button type="button" class="proposalclose" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button> -->
            </div>
            <div class="modal-body">

                <div id="breakup_msg">
                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                    <?= $this->lang->line('disabled_cont'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button id="activateBtn" data-dismiss="modal" onclick="activateUserPopup()" class="btn btn-info popup_ok" onmouseover="tooltip.pop(this, '#activate', {offsetY: -20, smartPosition: false})"><span class="propose-nav"><?= $this->lang->line('activate_now'); ?></span></button>
            </div>
        </div>
    </div>
</div>
<!-- end -->	

<script type="text/javascript">
	$( '#disabled' ).modal();
</script>

<?php 
} 
?>